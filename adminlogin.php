<?php
session_start();
error_reporting(0);
include 'includes/config.php';
if(isset($_POST['signin']))
{
$Usernameadmin=$_POST['Usernameadmin'];
$Passwordadmin=md5($_POST['Passwordadmin']);

$sql ="SELECT  Username, Password FROM polite_admin WHERE Username=:Usernameadmin AND Password=:Passwordadmin";
$query= $dbh -> prepare($sql);
$query-> bindParam(':Usernameadmin', $Usernameadmin, PDO::PARAM_STR);
$query-> bindParam(':Passwordadmin', $Passwordadmin, PDO::PARAM_STR);
$query-> execute();
$results=$query->fetchAll(PDO::FETCH_OBJ);
if($query->rowCount() > 0)
{

$_SESSION['Usernameadmin']=$_POST['Usernameadmin'];
if (!empty($_POST['save'])) {
    setcookie('Usernameadmin',$_POST['Usernameadmin'],time()+(10*365*60*60));
    setcookie('Passwordadmin',$_POST['Passwordadmin'],time()+(10*365*60*60));

}else{
    if(isset($_COOKIE['Usernameadmin'])){
        setcookie('Usernameadmin','');

        if(isset($_COOKIE['Passwordadmin'])){
            setcookie('Passwordadmin','');
        }
    }
}
echo "<script type='text/javascript'> document.location = 'admin/index.php'; </script>";
 }

else{

  echo "<script>alert('Invalid Details');</script>";

}

}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Kanit:wght@100;200;300;400;500&family=Roboto:ital,wght@0,100;0,400;0,500;0,700;0,900;1,500;1,700;1,900&display=swap" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>เข้าสู่ระบบสำหรับเจ้าหน้าที่</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <script src="includes/js/bootstrap.js"></script>  
</head>
<style>
  *{

  }

</style>
<?php include 'includes/header.php'; ?>

<body>
  <main>
<div class="p-3 mb-2 bg-secondary text-white"><div class="d-flex justify-content-between">
<div class="item" style="margin-right:  50px; font-size: 20px;">
                    <a href="#" class="nav-link  text-white">เข้าสู่ระบบผู้ดูแล</a>
                </div>
<div class="item" style="margin-right:  50px; font-size: 20px;">
                    <a href="index.php" class="nav-link  text-white">หน้าหลัก</a>
                </div>
 </div> 
</div>
<div class="container">
  <br>
  <br>
  <img src="admin/img/main/admin.png" class="rounded mx-auto d-block" alt="..." width ="300px">
  <br>
  <br>
  <form  method="post">
  <div class="row mb-3">
    <label for="Username" class="col-sm-2 col-form-label-lg ">Username</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" value="<?php if(isset($_COOKIE['Usernameadmin'])){ echo $_COOKIE['Usernameadmin']; } ?>" id="Usernameadmin" name="Usernameadmin" placeholder="กรอก Username "require>
    </div>
  </div>
  <div class="row mb-3">
    <label for="Password" class="col-sm-2 col-form-label-lg">Password</label>
    <div class="col-sm-10">
      <input type="password" class="form-control" id="Passwordadmin" value="<?php if(isset($_COOKIE['Passwordadmin'])){ echo $_COOKIE['Passwordadmin']; } ?>" placeholder="กรอก Password " name="Passwordadmin" require>
    </div>
  </div>

  <div class="row mb-3">
    <div class="col-sm-10 offset-sm-2">
      <div class="form-check">
        <input class="form-check-input" type="checkbox" id="Check" name="save" value="save" <?php if(isset($_COOKIE['Usernameadmin'])){ ?> checked<?php } ?>>
        <label class="form-check-label" for="Check">
          จำรหัสผ่าน
        </label>
      </div>
    </div>
  </div>
  
  <div class="text-center">
  <button type="submit" name="signin" class="btn-lg btn-success me-2">เข้าสู่ระบบ</button>
    </div>
</form>

<div>
  <br>
  </main>
</body>
<?php include 'includes/footer.php'?>
</html>