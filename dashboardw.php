<?php
session_start();
error_reporting(0);
include 'includes/config.php';
if (strlen($_SESSION['Username']) == 0) {
    header('location:index.php');
} else {
    if (isset($_POST['submitdb'])) {
        $week = $_POST['week'];
    } else {
        $week = 1;
    }


?>

    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Kanit:wght@100;200;300;400;500&family=Roboto:ital,wght@0,100;0,400;0,500;0,700;0,900;1,500;1,700;1,900&display=swap" rel="stylesheet">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>ABC Health Fitness</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <script src="includes/js/bootstrap.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.bundle.min.js"></script>
        <?php
        $dataw[10];
        $datah[10];
        $datac[10];
        $databmi[10];
        $dayi[10];
        $Username = $_SESSION['Username'];
        $sql = "SELECT w,h,bmi,waistline,days FROM polite_prescription_new WHERE Week=:week AND Username=:Username";
        $query = $dbh->prepare($sql);
        $query->bindParam(':Username', $Username, PDO::PARAM_STR);
        $query->bindParam(':week', $week, PDO::PARAM_STR);
        //  $query->bindParam(':Week',$Username,PDO::PARAM_STR);
        $query->execute();
        $results = $query->fetchAll(PDO::FETCH_OBJ);

        if ($query->rowCount() > 0) {
            $i = 0;
            foreach ($results as $result) {

                $dataw[$i] = $result->w;
                $datah[$i] = $result->h;
                $databmi[$i] = $result->bmi;
                $datac[$i] = $result->waistline;
                $dayi[$i] = $result->days;
                $i++;
            }
        }



        ?>
        <!DOCTYPE HTML>
        <html>

        <head>
            <script>
                window.onload = function() {

                    var chart = new CanvasJS.Chart("chartContainer", {
                        animationEnabled: true,
                        title: {
                            text: "น้ำหนัก ส่วนสูง รอบเอวและ BMI"
                        },
                        axisY: {
                            title: "กิโลกรัม หรือ เซ็นติเมตร",
                            titleFontColor: "#4F81BC",
                            lineColor: "#4F81BC",
                            labelFontColor: "#4F81BC",
                            tickColor: "#4F81BC"
                        },
                        toolTip: {
                            shared: true
                        },
                        legend: {
                            cursor: "pointer",
                            itemclick: toggleDataSeries
                        },
                        data: [{
                                type: "column",
                                name: "น้ำหนัก",
                                legendText: "น้ำหนัก",
                                showInLegend: true,
                                dataPoints: [{
                                        label: <?php echo $dayi[0] ?>,
                                        y: <?php echo $dataw[0] ?>
                                    },
                                    {
                                        label: <?php echo $dayi[1] ?>,
                                        y: <?php echo $dataw[1] ?>
                                    },
                                    {
                                        label: <?php echo $dayi[2] ?>,
                                        y: <?php echo $dataw[2] ?>
                                    },
                                    {
                                        label: <?php echo $dayi[3] ?>,
                                        y: <?php echo $dataw[3] ?>
                                    },
                                    {
                                        label: <?php echo $dayi[4] ?>,
                                        y: <?php echo $dataw[4] ?>
                                    },
                                    {
                                        label: <?php echo $dayi[5] ?>,
                                        y: <?php echo $dataw[5] ?>
                                    },
                                    {
                                        label: <?php echo $dayi[6] ?>,
                                        y: <?php echo $dataw[6] ?>
                                    },
                                ]
                            },
                            {
                                type: "column",
                                name: "ส่วนสูง",
                                legendText: "ส่วนสูง",
                                showInLegend: true,
                                dataPoints: [{
                                        label: <?php echo $dayi[0] ?>,
                                        y: <?php echo $datah[0] ?>
                                    },
                                    {
                                        label: <?php echo $dayi[1] ?>,
                                        y: <?php echo $datah[1] ?>
                                    },
                                    {
                                        label: <?php echo $dayi[2] ?>,
                                        y: <?php echo $datah[2] ?>
                                    },
                                    {
                                        label: <?php echo $dayi[3] ?>,
                                        y: <?php echo $datah[3] ?>
                                    },
                                    {
                                        label: <?php echo $dayi[4] ?>,
                                        y: <?php echo $datah[4] ?>
                                    },
                                    {
                                        label: <?php echo $dayi[5] ?>,
                                        y: <?php echo $datah[5] ?>
                                    },
                                    {
                                        label: <?php echo $dayi[6] ?>,
                                        y: <?php echo $datah[6] ?>
                                    },
                                ]
                            },
                            {
                                type: "column",
                                name: "รอบเอว",
                                legendText: "รอบเอว",
                                showInLegend: true,
                                dataPoints: [{
                                        label: <?php echo $dayi[0] ?>,
                                        y: <?php echo $datac[0] ?>
                                    },
                                    {
                                        label: <?php echo $dayi[1] ?>,
                                        y: <?php echo $datac[1] ?>
                                    },
                                    {
                                        label: <?php echo $dayi[2] ?>,
                                        y: <?php echo $datac[2] ?>
                                    },
                                    {
                                        label: <?php echo $dayi[3] ?>,
                                        y: <?php echo $datac[3] ?>
                                    },
                                    {
                                        label: <?php echo $dayi[4] ?>,
                                        y: <?php echo $datac[4] ?>
                                    },
                                    {
                                        label: <?php echo $dayi[5] ?>,
                                        y: <?php echo $datac[5] ?>
                                    },
                                    {
                                        label: <?php echo $dayi[6] ?>,
                                        y: <?php echo $datac[6] ?>
                                    },
                                ]
                            },
                            {
                                type: "column",
                                name: "BMI",
                                legendText: "BMI",
                                showInLegend: true,
                                dataPoints: [{
                                        label: <?php echo $dayi[0] ?>,
                                        y: <?php echo $databmi[0] ?>
                                    },
                                    {
                                        label: <?php echo $dayi[1] ?>,
                                        y: <?php echo $databmi[1] ?>
                                    },
                                    {
                                        label: <?php echo $dayi[2] ?>,
                                        y: <?php echo $databmi[2] ?>
                                    },
                                    {
                                        label: <?php echo $dayi[3] ?>,
                                        y: <?php echo $databmi[3] ?>
                                    },
                                    {
                                        label: <?php echo $dayi[4] ?>,
                                        y: <?php echo $databmi[4] ?>
                                    },
                                    {
                                        label: <?php echo $dayi[5] ?>,
                                        y: <?php echo $databmi[5] ?>
                                    },
                                    {
                                        label: <?php echo $dayi[6] ?>,
                                        y: <?php echo $databmi[6] ?>
                                    },
                                ]
                            },
                        ]
                    });
                    chart.render();

                    function toggleDataSeries(e) {
                        if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
                            e.dataSeries.visible = false;
                        } else {
                            e.dataSeries.visible = true;
                        }
                        chart.render();
                    }

                }
            </script>
        </head>
        <style>
            * {}
        </style>
        <?php include 'includes/headerwork.php'; ?>

    <body>
        <main>
            <div class="container">
                <div class="container px-4">
                    <form method="post">
                        <div class="row gx-5">
                            <div class="col">
                                <div class="p-3 "> <select class="form-select" aria-label="Default select example" name="week">
                                        
                                        <?php
                                        for ($x = 1; $x <= 12; $x++) {
                                            if($_POST['week']==$x){
                                                ?>
                                                <option value="<?php echo $x ?>" selected> <?php echo "สัปดาห์ ".$x ?></option>
                                                <?php
                                            }else{
                                                ?>
                                        
                                                <option value="<?php echo $x ?>"><?php echo "สัปดาห์ ".$x ?></option>
                                            <?php
                                            }
                                        
                                        }
                                        ?>
                                    </select></div>
                            </div>
                            <div class="col">
                                <div class="p-3 "><input type="submit" value="รายงาน" name="submitdb" id="submitdb" class="btn btn-warning"></div>
                            </div>
                        </div>
                    </form>
                </div>
                <br>
                <div id="chartContainer" style="height: 500px; width: 100%; " class="mb-5"></div>
                <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>

            </div>



            </div>
        </main>

    </body>
    <?php include 'includes/footer.php' ?>

    </html>

<?php
}
?>