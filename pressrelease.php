<?php
include 'includes/config.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Kanit:wght@100;200;300;400;500&family=Roboto:ital,wght@0,100;0,400;0,500;0,700;0,900;1,500;1,700;1,900&display=swap" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>หัวข้อข่าวประชาสัมพันธ์</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <script src="includes/js/bootstrap.js"></script>  
</head>
<style>
  *{

  }

</style>
<?php include 'includes/header.php'; ?>
<div class="p-3 mb-2 bg-secondary text-white"><div class="d-flex justify-content-between">
<div class="item" style="margin-right:  50px; font-size: 20px;">
                    <a href="#" class="nav-link  text-white">หัวข้อข่าวประชาสัมพันธ์</a>
                </div>
<div class="item" style="margin-right:  50px; font-size: 20px;">
                    <a href="index.php" class="nav-link  text-white">หน้าหลัก</a>
                </div>
 </div> </div>

<body>
  
<main class="container">
  
<?php
                        if(isset($_GET['Id'])){
                           
                          $Id=$_GET['Id'];
                          $sql = "SELECT * from polite_pressrelease WHERE Id=:Id";
                          $query = $dbh -> prepare($sql);
                          $query -> bindParam(':Id',$Id, PDO::PARAM_STR);
                          $query->execute();
                          $results=$query->fetchAll(PDO::FETCH_OBJ);
                       
                    
                        if($query->rowCount() > 0)
                        {
                          foreach($results as $result)
                          { 
                               ?>
                              <div class="row mb-2">
    <div class="col-md-6 p-5">
      <div class="row g-0 border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
      <img class="img-responsive"  src="admin/img/<?php echo htmlentities($result->Image_file);?>" width="700" height="700">

      </div>
    </div>
    <div class="col-md-6 p-5">
      <div class="row g-0 border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
        <div class="col p-4 d-flex flex-column position-static">
          
          <h3 class="mb-0"><?php echo htmlentities($result->Section);?></h3>
          <br>
          <hr>
          <?php
	function DateThai($strDate)
	{
		$strYear = date("Y",strtotime($strDate))+543;
		$strMonth= date("n",strtotime($strDate));
		$strDay= date("j",strtotime($strDate));
		$strHour= date("H",strtotime($strDate));
		$strMinute= date("i",strtotime($strDate));
		$strSeconds= date("s",strtotime($strDate));
		$strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
		$strMonthThai=$strMonthCut[$strMonth];
		return "$strDay $strMonthThai $strYear, $strHour:$strMinute น.";
	}

   
	
?>
          <div class="mb-1 text-muted" style='word-break:break-all'><?php echo DateThai($result->Datetime) ?></div>
        
        </div>
    
      </div>
      <div class="row g-0 border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
        <div class="col p-4 d-flex flex-column position-static">

          <h3 class="mb-0">รายละเอียด</h3>
          <div class="mb-1 text-muted"></div>
          <p class="mb-auto" style='word-break:break-all'><?php echo htmlentities($result->News_details) ?></p>
          <p class="mb-auto" style='word-break:break-all'> Link : <a class="mb-auto" style='word-break:break-all' href='<?php echo htmlentities($result->Link) ?>'><?php echo htmlentities($result->Link) ?></a></p>
          
        </div>
    
      </div>
    </div>
  </div>
                               <?php
                          }
                        }
                        }
                        ?>

                        
  
  </main>
   
</body>
<?php include 'includes/footer.php'?>
</html>