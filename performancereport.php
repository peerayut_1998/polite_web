<?php
session_start();
error_reporting(0);
include 'includes/config.php';
if (strlen($_SESSION['Username']) == 0) {
    header('location:index.php');
} else {
    $Username = $_SESSION['Username'];
    $Gday = $_SESSION['Gday'];

    if (isset($_POST['editdata'])) {

        $t1 = $_POST['1'];
        $t2 = $_POST['2'];
        $t3 = $_POST['3'];
        $t4 = $_POST['4'];
        $t5 = $_POST['5'];
        $t6 = $_POST['6'];
        $t7 = $_POST['7'];
        $t8 = $_POST['8'];
        $t9 = $_POST['9'];
        $w = $_POST['weight'];
        $h = $_POST['height'];
        $bmi = $w / (($h / 100) ** 2);

        $waistline = $_POST['waistline'];
        $Textarea = $_POST['Textarea'];
        if ($bmi > 0) {
            $sql = "UPDATE polite_prescription_new SET t1=:t1,t2=:t2,t3=:t3,t4=:t4,t5=:t5,t6=:t6,t7=:t7,t8=:t8,t9=:t9,w=:w,h=:h,bmi=:bmi,waistline=:waistline  WHERE days=:days AND Username=:Username;
       UPDATE polite_obstacle SET details=:Textarea WHERE days=:days AND Username=:Username;";
            $query = $dbh->prepare($sql);
            $query->bindParam(':days', $Gday, PDO::PARAM_STR);
            $query->bindParam(':Username', $Username, PDO::PARAM_STR);
            $query->bindParam(':t1', $t1, PDO::PARAM_STR);
            $query->bindParam(':t2', $t2, PDO::PARAM_STR);
            $query->bindParam(':t3', $t3, PDO::PARAM_STR);
            $query->bindParam(':t4', $t4, PDO::PARAM_STR);
            $query->bindParam(':t5', $t5, PDO::PARAM_STR);
            $query->bindParam(':t6', $t6, PDO::PARAM_STR);
            $query->bindParam(':t7', $t7, PDO::PARAM_STR);
            $query->bindParam(':t8', $t8, PDO::PARAM_STR);
            $query->bindParam(':t9', $t9, PDO::PARAM_STR);
            $query->bindParam(':w', $w, PDO::PARAM_STR);
            $query->bindParam(':h', $h, PDO::PARAM_STR);
            $query->bindParam(':bmi', $bmi, PDO::PARAM_STR);
            $query->bindParam(':waistline', $waistline, PDO::PARAM_STR);
            $query->bindParam(':Textarea', $Textarea, PDO::PARAM_STR);
            $query->execute();

            header('location:report.php');
        } else {
            echo "<script type='text/javascript'>alert('คุณใส่ bmi ไม่ถูกต้อง');</script>";
        }
    }

?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Kanit:wght@100;200;300;400;500&family=Roboto:ital,wght@0,100;0,400;0,500;0,700;0,900;1,500;1,700;1,900&display=swap" rel="stylesheet">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>ABC Health Fitness</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <script src="includes/js/bootstrap.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.bundle.min.js"></script>
        <?php
        $Username = $_SESSION['Username'];
        ?>
        <!DOCTYPE HTML>
        <html>

        <head>
        </head>
        <style>
            * {}
        </style>
        <?php include 'includes/headerwork.php'; ?>

    <body>
        <main>
            <div class="p-3 mb-2 bg-secondary text-white">
                <div class="d-flex justify-content-between">
                    <div class="item" style="margin-right:  50px; font-size: 20px;">
                        <a href="#" class="nav-link  text-white">หลักการปฏิบัติตนเอง</a>
                    </div>
                    <div class="item" style="margin-right:  50px; font-size: 20px;">
                        <a href="index.php" class="nav-link  text-white">หน้าหลัก</a>
                    </div>
                </div>
            </div>
            <div class="container">
                <br>
                <div class="d-flex flex-column justify-content-center">
                    <div class="d-flex justify-content-center">
                        <h5>
                            6 เทคนิคดีๆ
                        </h5>
                    </div>
                    <div class="d-flex justify-content-center">
                        <div class="embed-responsive embed-responsive-4by3" style="width:600px;height:400px">
                            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/EjfJW4V4QTE"></iframe>
                        </div>
                    </div>
                    <div class="d-flex justify-content-center mt-3">
                        <h4>
                            ทำแบบบันทึกหลักการปฏิบัติสุขภาพร่างกาย 12 สัปดาห์
                        </h4>
                    </div>
                    <div class="d-flex justify-content-center">
                        <form method="POST">
                            <div class="d-flex flex-column" style="font-size: 18px;">
                                <?php
                                if ($Gday > 85) {
                                } else {
                                    $sql = "SELECT * FROM polite_prescription_new WHERE days=:days AND Username=:Username";
                                    $query = $dbh->prepare($sql);
                                    $query->bindParam(':days', $Gday, PDO::PARAM_STR);
                                    $query->bindParam(':Username', $Username, PDO::PARAM_STR);
                                    $query->execute();
                                    $results = $query->fetchAll(PDO::FETCH_OBJ);
                                    if ($query->rowCount() > 0) {
                                        foreach ($results as $result) {
                                ?>
                                            <div class="mt-2">
                                                <h5>วันที่ <?php echo htmlentities($result->days) ?> สัปดาห์ที่ <?php echo htmlentities($result->Week) ?> ทำแบบบันทึกวันต่อวัน
                                                </h5>
                                                <div class="d-flex " style="background-color:rgba(196, 196, 196, 1);">
                                                    <h6 class="mx-2 my-2">
                                                        <?php echo date("d-m-Y"); ?>
                                                    </h6>
                                                </div>
                                            </div>
                                            <div class="mt-2">
                                                1. ลดอาหารกลุ่มคาร์โบไฮเดรต เช่น ข้าว ขนมปัง ขนมหวาน ( 3 มื้อ )
                                                <div class=" d-flex justify-content-around " style="background-color:rgba(196, 196, 196, 1);">
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="1" id="1" value="y" <?php if ($result->t1 == "y") {
                                                                                                                                    echo "checked";
                                                                                                                                } ?> required>
                                                        <label class="form-check-label" for="1">ทำ</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="1" id="inlineRadio2" value="n" <?php if ($result->t1 == "n") {
                                                                                                                                                echo "checked";
                                                                                                                                            } ?> required>
                                                        <label class="form-check-label" for="1">ไม่ทำ</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="mt-2">
                                                2. เพิ่มอาหารโปรตีนคุณภาพดี เช่น นม ไข่ต้ม ปลา ( 3มื้อ )
                                                <div class=" d-flex justify-content-around" style="background-color:rgba(196, 196, 196, 1);">
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="2" id="2" value="y" <?php if ($result->t2 == "y") {
                                                                                                                                    echo "checked";
                                                                                                                                } ?> required>
                                                        <label class="form-check-label" for="2">ทำ</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="2" id="inlineRadio2" value="n" <?php if ($result->t2 == "n") {
                                                                                                                                                echo "checked";
                                                                                                                                            } ?> required>
                                                        <label class="form-check-label" for="2">ไม่ทำ</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="mt-2">
                                                3. ไม่รับประทานอาหารเย็นหลัง 20.00 น.
                                                <div class=" d-flex justify-content-around" style="background-color:rgba(196, 196, 196, 1);">
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="3" id="3" value="y" <?php if ($result->t3 == "y") {
                                                                                                                                    echo "checked";
                                                                                                                                } ?> required>
                                                        <label class="form-check-label" for="3">ทำ</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="3" id="inlineRadio2" value="n" <?php if ($result->t3 == "n") {
                                                                                                                                                echo "checked";
                                                                                                                                            } ?> required>
                                                        <label class="form-check-label" for="3">ไม่ทำ</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="mt-2">
                                                4. กินข้าวเสร็จแล้วไม่กินขนมหวานต่อ หรือของกินเล่น ( 3มื้อ )
                                                <div class="d-flex justify-content-around" style="background-color:rgba(196, 196, 196, 1);">
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="4" id="4" value="y" <?php if ($result->t4 == "y") {
                                                                                                                                    echo "checked";
                                                                                                                                } ?> required>
                                                        <label class="form-check-label" for="4">ทำ</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="4" id="inlineRadio2" value="n" <?php if ($result->t4 == "n") {
                                                                                                                                                echo "checked";
                                                                                                                                            } ?> required>
                                                        <label class="form-check-label" for="4">ไม่ทำ</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="mt-2">
                                                5. ดื่มน้ำเปล่าวันละ 2 ลิตร หรือ 8 - 10 แก้ว
                                                <div class="d-flex justify-content-around" style="background-color:rgba(196, 196, 196, 1);">
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="5" id="5" value="y" <?php if ($result->t5 == "y") {
                                                                                                                                    echo "checked";
                                                                                                                                } ?> required>
                                                        <label class="form-check-label" for="5">ทำ</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="5" id="inlineRadio2" value="n" <?php if ($result->t5 == "n") {
                                                                                                                                                echo "checked";
                                                                                                                                            } ?> required>
                                                        <label class="form-check-label" for="5">ไม่ทำ</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="mt-2">
                                                6. เน้นอาหารต้ม นึ่ง ลดอาหาร ทอด หวาน มัน เค็ม ( 3 มื้อ )
                                                <div class="d-flex justify-content-around" style="background-color:rgba(196, 196, 196, 1);">
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="6" id="6" value="y" <?php if ($result->t6 == "y") {
                                                                                                                                    echo "checked";
                                                                                                                                } ?> required>
                                                        <label class="form-check-label" for="6">ทำ</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="6" id="inlineRadio2" value="n" <?php if ($result->t6 == "n") {
                                                                                                                                                echo "checked";
                                                                                                                                            } ?> required>
                                                        <label class="form-check-label" for="6">ไม่ทำ</label>
                                                    </div>

                                                </div>
                                            </div>

                                            <div class="mt-2">
                                                7. ออกกำลังกายได้ 20 - 30 นาที ต่อครั้ง
                                                <div class="d-flex justify-content-around" style="background-color:rgba(196, 196, 196, 1);">
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="7" id="7" value="y" <?php if ($result->t7 == "y") {
                                                                                                                                    echo "checked";
                                                                                                                                } ?> required>
                                                        <label class="form-check-label" for="7">ทำ</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="7" id="inlineRadio2" value="n" <?php if ($result->t7 == "n") {
                                                                                                                                                echo "checked";
                                                                                                                                            } ?> required>
                                                        <label class="form-check-label" for="7">ไม่ทำ</label>
                                                    </div>

                                                </div>
                                            </div>

                                            <div class="mt-2">
                                                8. ลักษณะออดกำลังกายมีเหงื่อซึม และหายใจเร็วกว่าปกติ
                                                <div class="d-flex justify-content-around" style="background-color:rgba(196, 196, 196, 1);">
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="8" id="8" value="y" <?php if ($result->t8 == "y") {
                                                                                                                                    echo "checked";
                                                                                                                                } ?> required>
                                                        <label class="form-check-label" for="8">ทำ</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="8" id="inlineRadio2" value="n" <?php if ($result->t8 == "n") {
                                                                                                                                                echo "checked";
                                                                                                                                            } ?> required>
                                                        <label class="form-check-label" for="8">ไม่ทำ</label>
                                                    </div>

                                                </div>
                                            </div>

                                            <div class="mt-2">
                                                9. ออกกำลังกายท่าบริหารกระชับเอว พุง
                                                <div class="d-flex justify-content-around" style="background-color:rgba(196, 196, 196, 1);">
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="9" id="9" value="y" <?php if ($result->t9 == "y") {
                                                                                                                                    echo "checked";
                                                                                                                                } ?> required>
                                                        <label class="form-check-label" for="9">ทำ</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="radio" name="9" id="inlineRadio2" value="n" <?php if ($result->t9 == "n") {
                                                                                                                                                echo "checked";
                                                                                                                                            } ?> required>
                                                        <label class="form-check-label" for="9">ไม่ทำ</label>
                                                    </div>

                                                </div>
                                            </div>

                            </div>

                    </div>

                </div>

                <div class="my-5">
                    <div style="background-color:#CC9999"><label for="Textarea">
                            <p style="margin: 10px 10px; font-size:16px">ปัญหาอุปสรรค ในการเปลี่ยนแปลงของคุณ</p>
                        </label>
                        <?php


                                            $sql = "SELECT * FROM polite_obstacle WHERE Username=:Username AND days=:days";
                                            $query = $dbh->prepare($sql);
                                            $query->bindParam(':Username', $Username, PDO::PARAM_STR);
                                            $query->bindParam(':days', $Gday, PDO::PARAM_INT);
                                            $query->execute();
                                            $resultss = $query->fetchAll(PDO::FETCH_OBJ);

                                            if ($query->rowCount() > 0) {

                                                foreach ($resultss as $results) {
                        ?>
                                <textarea class="form-control" id="Textarea" name="Textarea" rows="3"> <?php echo htmlentities($results->details) ?> </textarea>
                        <?php
                                                }
                                            }

                        ?>

                    </div>

                    <br>
                    <div class="form-row">
                        <div class="col-md-4">
                            <label for="weight">น้ำหนัก </label>
                            <input type="number" class="form-control" id="weight" name="weight" placeholder="weight" value="<?php echo (int)$result->w; ?>" oninput="calculate()" required>
                        </div>
                        <div class="col-md-4 offset-md-4">
                            <label for="height">ส่วนสูง</label>
                            <input type="number" class="form-control" id="height" name="height" placeholder="height" value="<?php echo (int)$result->h; ?>" oninput="calculate()" required>
                        </div>
                    </div>
                    <br>
                    <div class="form-row">
                        <div class="col-md-4">
                            <label for="bmi">BMI</label>
                            <input type="text" class="form-control" id="bmi" name="bmi" placeholder="bmi" value="<?php echo (int)$result->bmi; ?>" disabled>

                        </div>
                        <div class="col-md-4 offset-md-4">
                            <label for="waistline">รอบเอว</label>
                            <input type="number" class="form-control" name="waistline" id="waistline" value="<?php echo (int)$result->waistline; ?>" placeholder="waistline" required>
                        </div>
                    </div>
                    <br>
                    <div class="container" style="padding-top:10px ;">
                        <div class="row">
                            <div class="col text-center">
                                <button type="submit" class="btn btn-warning btn-lg" name="editdata" id="editdata" onclick="alert('บันทึกข้อมูลเสร็จสิ้น ')">บันทึก</button>
                            </div>
                        </div>
                    </div>



                </div>

                </form>
    <?php
                                        }
                                    }
                                }

    ?>
            </div>
        </main>
        <script>
            function calculate() {
                var bmi;
                var result = document.getElementById("bmi");
                var weight = parseInt(document.getElementById("weight").value);
                var height = parseInt(document.getElementById("height").value);
                bmi = (weight / Math.pow((height / 100), 2)).toFixed(1);

                if (height === "" || isNaN(height))
                    result.value = "0";

                else if (weight === "" || isNaN(weight))
                    result.value = "0";

                // If both input is valid, calculate the bmi
                else {

                    // Fixing upto 2 decimal places
                    let bmi = (weight / ((height * height) / 10000)).toFixed(2);
                    result.value = bmi;
                    // Dividing as per the bmi conditions
                }
            }
        </script>
    </body>
    <?php include 'includes/footer.php' ?>

    </html>

<?php
}
?>