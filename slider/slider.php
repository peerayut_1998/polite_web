<?php
session_start();
error_reporting(0);
include '../includes/config.php';
if(strlen($_SESSION['Username'])==0)
    {  
      header('location:../index.php');
    }else{
        ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <!--meta name="viewport" content="width=device-width; initial-scale=1.0" /-->
        <link rel="stylesheet" type="text/css" media="screen" href="css/style.css" />
        <link rel="stylesheet" type="text/css" media="screen" href="css/fullscreen_slider.css" />
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
        <script type="text/javascript" src="http://code.jquery.com/jquery-1.9.1.js"></script>
        <script type="text/javascript" src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
        <script type="text/javascript" src="js/jquery.touchswipe.js"></script>
        <title>ข้อมูลแนะนำ</title>
        <script type="text/javascript" src="js/fullscreen_slider.js"></script>
        <script type="text/javascript">
        (function($){
            $(document).ready(function (){
                $('#slider').fullscreen_slider({
                easing: 'easeOutQuad',
                handle_width: 30, //Prev next show width
                speed: 'slow'
                });
            });
            $(window).resize(function() {
                $('#slider').fullscreen_slider('resize');
            });
        })(window.jQuery);
        </script>

    </head>
    
    <?php include 'headerwork.php'; ?>
    <body>
    <main>
    <div id="slider" class="fullscreen_slider">

    <?php $sql = "SELECT * from polite_recommend";
$query = $dbh -> prepare($sql);
$query->execute();
$results=$query->fetchAll(PDO::FETCH_OBJ);
$cnt=1;
if($query->rowCount() > 0)
{
    foreach($results as $result)
{  
?>
    <?php
    if($cnt==$query->rowCount()){
        ?>
        <div class="fullscreen_slider_child">
        <h5><?php echo htmlentities($result->Name);?></h5>
                <div class="fullscreen_slider_image " >
                    <img src="../admin/img/<?php echo htmlentities($result->Image_file);?>" />
                </div>
                <a href="../work.php?Week=1"> <div class="fullscreen_slider_caption"><button type="button" class="btn btn-primary">ถัดไป</button></div></a>
            </div>  
        <?php
    }else{
    
    ?>
            <div class="fullscreen_slider_child">
            <h5><?php echo htmlentities($result->Name);?></h5>
                <div class="fullscreen_slider_image " >
                    <img src="../admin/img/<?php echo htmlentities($result->Image_file);?>" />
                </div>
                <div class="fullscreen_slider_caption"><button type="button" class="btn btn-primary">ถัดไป</button></div>
            </div>   
<?php
}
$cnt++;
}
}else{
    ?>
    <div class="fullscreen_slider_child">
    <h5>ยังไม่มีข้อมูล</h5>
                <div class="fullscreen_slider_image " >
                    <img src="800x600.gif" />
                </div>
                <a href="../work.php?Week=1"> <div class="fullscreen_slider_caption"><button type="button" class="btn btn-primary">ถัดไป</button></div></a>
            </div>  
    <?php
}
?>
        
    
            
        </div>
        <br>
    </main>
    </body>
    <?php include 'footer.php'; ?>
</html>

<?php
    }
?>