

<head>


<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Kanit:wght@100;200;300;400;500&family=Roboto:ital,wght@0,100;0,400;0,500;0,700;0,900;1,500;1,700;1,900&display=swap" rel="stylesheet">
<meta name="viewport" content="width=device-width, initial-scale=1">
<?php include 'css/bootstrap5.php'?>
<style>

*{
  margin: 0;
    font-family:'Kanit';
}
html, body {
  height: 100%;
}
</style>
</head>

<!-- Footer -->
<footer class=" text-white " style="background-color:#999999;">
  <!-- Grid container -->
  <div class="container p-4">


    <!-- Section: Links -->
    <section class="">
      <!--Grid row-->
      <div class="row">
        <!--Grid column-->
        <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
          <h4 >การติดต่อหน่วยงาน</h4>
          <p>โรงพยาบาลส่งเสริมสุขภาพแม่และเด็ก
ศูนย์อนามัยที่ 3 นครสวรรค์ เทศบาลนครสวรรค์
จังหวัดนครรสวรรค์</p>

          <ul class="list-unstyled mb-0">
            <li>
              <a href="#!" class="text-white">Phone:056 255 451 ต่อ 154</a>
            </li>
            <li>
              <a href="#!" class="text-white">Email: abcfitness_hpc3@gmail.com</a>
            </li>
            <li>
              <a href="#!" class="text-white">Facebook:ABC Health Fitness</a>
            </li>
        
          </ul>
        </div>
        <!--Grid column-->

        <!--Grid column-->
        <div class="col-lg-3 col-md-6 mb-4 mb-md-0 ">
          <h5 class="text-uppercase">เมนู</h5>

          <ul class="list-unstyled mb-0">
            <li>
              <a href="#!" class="text-white">> หน้าหลัก</a>
            </li>
            <li>
              <a href="#!" class="text-white">> องค์ความร</a>
            </li>
            <li>
              <a href="#!" class="text-white">> ติดต่อเรา</a>
            </li>
        
          </ul>
        </div>
        <!--Grid column-->

        <!--Grid column-->
        <div class="col-lg-3 col-md-6 mb-4 mb-md-0 pr-5">
          <h5 class="text-uppercase">เว็บไซต์หลัก</h5>

          <ul class="list-unstyled mb-0">
            <li>
              <a href="#!" class="text-white">> ระบบการจัดการใบสั่งสุขภาพร่างกาย<br>ของ 
ABC Health Fitness ผ่านเว็บแบบตอบสนอ</a>
            </li>
        
          </ul>
        </div>
        <!--Grid column-->

        <!--Grid column-->
        <div class="col-lg-3 col-md-6 mb-4 mb-md-0 pl-5">
           <div >
              <img src="../admin/img/main/logo.png" width="100" height="125"alt="">
              <img src="../admin/img/main/441.png" width="100" height="125"alt="">
              </div>
              <div >
              
              <img src="../admin/img/main/logoit4cd.png" width="250" height="125"alt="">
              </div>
             
    
        </div>
        <!--Grid column-->
      </div>
      <!--Grid row-->
    </section>
    <!-- Section: Links -->

  </div>
  <!-- Grid container -->
ิ   <hr style="height:4px;border-width:0;color:gray;background-color:gray" width="60%">
  <!-- Copyright -->
  <div class=" container" >
  <div class="container">
    </div>
  <p  class ="pb-3 " >
  คณะวิทยาศาสตร์ สาขาเทคโนโลยีสารสนเทศ มหาลัยราภัฎนครสวรรค์
</p>
  </div>
  <!-- Copyright -->

</footer>
<!-- Footer -->