<?php
include 'includes/config.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Kanit:wght@100;200;300;400;500&family=Roboto:ital,wght@0,100;0,400;0,500;0,700;0,900;1,500;1,700;1,900&display=swap" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>หน้าหลัก</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <script src="includes/js/bootstrap.js"></script>  
</head>
<style>
  *{

  }

</style>
<?php include 'includes/header.php'; ?>

<body>
  
    <div class="container">
        <div class="row">
            <div class="col-lg-8 m-auto">
                <!-- การสร้าง Carousel -->
                <div class="carousel slide" id="slider1" data-bs-ride="carousel">
                        <ol class="carousel-indicators">
                            <button  data-bs-target="#slider1" data-bs-slide-to="0"></button>
                            <button  data-bs-target="#slider1" data-bs-slide-to="1"></button>
                            <button  class="active" data-bs-target="#slider1" data-bs-slide-to="2"></button>
                            <button  data-bs-target="#slider1" data-bs-slide-to="3"></button>
                        </ol>
                        <div class="carousel-inner">

                    
                      
                            <div class="carousel-item">
                                <img src="admin/img/main/ab1.jpg" class="d-block " width="100%" height="400px">
                                <div class="carousel-caption d-none d-md-block">
                                  
                                </div>
                            </div>
                            <div class="carousel-item">
                                <img src="admin/img/main/ab3.jpg" class="d-block "  width="100%" height="400px">
                                <div class="carousel-caption d-none d-md-block">
                                 
                                </div>
                            </div>
                            <div class="carousel-item active">
                                <img src="admin/img/main/he4.jpg" class="d-block "  width="100%" height="400px">
                                <div class="carousel-caption d-none d-md-block">
                                   
                                </div>
                            </div>
                            <div class="carousel-item">
                                <img src="admin/img/main/health.jpg" class="d-block " width="100%" height="400px">
                                <div class="carousel-caption d-none d-md-block">
                                    
                                </div>
                            </div>
                        </div>
                        <!-- ควบคุมการ Slide ผ่านปุ่ม -->
                        <button class="carousel-control-prev" data-bs-slide="prev" data-bs-target="#slider1">
                            <span class="carousel-control-prev-icon"></span>
                        </button>
                        <button class="carousel-control-next" data-bs-slide="next" data-bs-target="#slider1">
                            <span class="carousel-control-next-icon"></span>
                        </button>
                </div>
            </div>
        </div>
    </div>
    <div class="d-flex justify-content-center bg-light text-dark"><p style="font-size: 30px; margin-top: 10px;">ข่าวประชาสัมพันธ์<p></div>

    <div class="album py-5 bg-light">
    <div class="container">

      <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3">
      <?php
                         $sql = "SELECT * from polite_pressrelease";
                        $query = $dbh -> prepare($sql);
                        $query->execute();
                        $results=$query->fetchAll(PDO::FETCH_OBJ);
                    
                        if($query->rowCount() > 0)
                        {
                          foreach($results as $result)
                          { 
                               ?>
                               <div class="col">
        <a href="pressrelease.php?Id=<?php echo htmlentities($result->Id);?>"> <img class="img-responsive"  src="admin/img/<?php echo htmlentities($result->Image_file);?>" width="350    " height="550"> </a>
        </div>
                               <?php
                          }
                        
                        }
                        ?>

      
        
      </div>
    </div>
  </div>
</body>
<footer >

<?php include 'includes/footer.php'?>
</footer>

</html>