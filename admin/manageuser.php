<?php
session_start();
error_reporting(0);
include '../includes/config.php';
if(strlen($_SESSION['Usernameadmin'])==0)
    {  
      header('location:../index.php');
    }else{
include '../includes/config.php';
if(isset($_GET['delete']))
{
$Username=$_GET['Username'];
echo $Username;

$Id=$_GET['delete'];
$sql = "DELETE FROM polite_user  WHERE Id=:Id;
        DELETE FROM polite_obstacle WHERE Username=:Username;
        DELETE FROM polite_prescription WHERE Username=:Username;";
$query = $dbh->prepare($sql);
$query -> bindParam(':Id',$Id, PDO::PARAM_STR);
$query -> bindParam(':Username',$Username, PDO::PARAM_STR);

$query -> execute();
$msg="ลบข้อมูลเสร็จสิ้น";
  header('location:manageuser.php');
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
<title>จัดการประชาสัมพันธ์</title>
<meta charset="utf-8">
<link rel="stylesheet" href="../includes/css/styles.css">
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Kanit:wght@100;200;300;400;500&family=Roboto:ital,wght@0,100;0,400;0,500;0,700;0,900;1,500;1,700;1,900&display=swap" rel="stylesheet">
<meta name="viewport" content="width=device-width, initial-scale=1">
<?php include '../includes/css/bootstrap5.php'?>

</head>
<body>

<?php include '../includes/admin/header.php'?>

<section>
<?php include '../includes/admin/sidebar.php'?>

  
<article>
    <h1>เพิ่มองค์ความรู้</h1>
    <hr>
   
    

    <br>
    <?php $sql = "SELECT * from polite_user";
$query = $dbh -> prepare($sql);
$query->execute();
$results=$query->fetchAll(PDO::FETCH_OBJ);
$cnt=1;
if($query->rowCount() > 0)
{
  
  ?>
    <div>
      
    <table class="table table-striped table-bordered">
  <thead>
    <tr>
      <th scope="col">ลำดับ</th>
      <th scope="col">ชื่อ</th>
      <th scope="col">นามสกุล</th>
      <th scope="col">เบอร์โทรศัพท์</th>
      <th scope="col">แก้ไข</th>
      <th scope="col">ลบ</th>

    </tr>
  </thead>
  <tbody>
  <?php 
foreach($results as $result)
{               ?> 
    <tr>
      <th scope="row"><?php echo htmlentities($cnt) ?></th>
      <td><?php echo htmlentities($result->First_Name) ?></td>
      <td><?php echo htmlentities($result->Last_Name) ?></td>
      <td><?php echo htmlentities($result->Phone_Number) ?></td>

      <td> <a href="edituser.php?edit=<?php echo htmlentities($result->Id);?>"><button type="button" class="btn btn-warning">แก้ไข</button></a></td>
      <td> <a href="manageuser.php?delete=<?php echo htmlentities($result->Id);?>&Username=<?php echo htmlentities($result->Username);?>" onclick="return confirm('Do you want to delete');"><button type="button" class="btn btn-danger">ลบ</button></a></td>
      
      
    </tr>
    
    <?php
 $cnt++;}
                                        
} 
    ?>
  </tbody>
</table>
    </div>
  </article>
</section>


</body>
</html>

<?php
    }

    ?>