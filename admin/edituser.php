<?php
session_start();
error_reporting(0);
include '../includes/config.php';
if(strlen($_SESSION['Usernameadmin'])==0)
    {  
      header('location:../index.php');
    }else{

if(isset($_POST['apply'])){
    if(isset($_POST['Password']))
    {
$Id=$_GET['edit'];
$First_Name=$_POST['First_Name'];
$Last_Name=$_POST['Last_Name'];
$Phone_Number=$_POST['Phone_Number'];
$Password=md5($_POST['Password']);
$sql="UPDATE polite_user SET Password=:Password,First_Name=:First_Name,Last_Name=:Last_Name,Phone_Number=:Phone_Number WHERE Id=:Id";
$query = $dbh->prepare($sql);
$query->bindParam(':Id',$Id,PDO::PARAM_STR);
$query->bindParam(':First_Name',$First_Name,PDO::PARAM_STR);
$query->bindParam(':Last_Name',$Last_Name,PDO::PARAM_STR);
$query->bindParam(':Phone_Number',$Phone_Number,PDO::PARAM_STR);
$query->bindParam(':Password',$Password,PDO::PARAM_STR);
$query->execute();

    }else{
        $Id=$_GET['edit'];
$First_Name=$_POST['First_Name'];
$Last_Name=$_POST['Last_Name'];
$Phone_Number=$_POST['Phone_Number'];

$sql="UPDATE polite_user SET First_Name=:First_Name,Last_Name=:Last_Name,Phone_Number=:Phone_Number WHERE Id=:Id";
$query = $dbh->prepare($sql);
$query->bindParam(':Id',$Id,PDO::PARAM_STR);
$query->bindParam(':First_Name',$First_Name,PDO::PARAM_STR);
$query->bindParam(':Last_Name',$Last_Name,PDO::PARAM_STR);
$query->bindParam(':Phone_Number',$Phone_Number,PDO::PARAM_STR);

$query->execute();
    }

}
?>


<!DOCTYPE html>
<html lang="en">
<head>
<title>แก้ไขข้อมูลสมัครชิก</title>
<meta charset="utf-8">
<link rel="stylesheet" href="../includes/css/styles.css">
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Kanit:wght@100;200;300;400;500&family=Roboto:ital,wght@0,100;0,400;0,500;0,700;0,900;1,500;1,700;1,900&display=swap" rel="stylesheet">
<meta name="viewport" content="width=device-width, initial-scale=1">
<?php include '../includes/css/bootstrap5.php'?>
</head>
<body>
<?php include '../includes/admin/header.php'?>
<section>
<?php include '../includes/admin/sidebar.php'?>


<article>

    <h1>แก้ไขข้อมูลสมัครชิก</h1>
    <hr>
    <?php
if(isset($_GET['edit'])){
    
$Id=$_GET['edit'];
 $sql = "SELECT * from polite_user WHERE Id=:Id";
$query = $dbh -> prepare($sql);
$query -> bindParam(':Id',$Id, PDO::PARAM_STR);
$query->execute();
$results=$query->fetchAll(PDO::FETCH_OBJ);
$cnt=1;
if($query->rowCount() > 0)
{

    foreach($results as $result)
{ 
    ?>
    <div>
    <form method="post" >
  <div class="form-group">
    
  <h3 class="text-danger">รหัสสมาชิก : <?php echo htmlentities($result->Code_User)?></h3>
  <h3 class="text-success">Username : <?php  echo htmlentities($result->Username)?></hh3>

  </div>
  <div class="form-group">
    <label for="inputName">ชื่อ</label>
    <input type="text" class="form-control" id="First_Name"  name="First_Name" value="<?php echo htmlentities($result->First_Name); ?>" placeholder="กรอกชื่อ" required>
  
  </div>
  
  <div class="form-group">
    <label for="inputName">นามสกุล</label>
    <input type="text" class="form-control" id="Last_Name"  name="Last_Name" value="<?php echo htmlentities($result->Last_Name); ?>" placeholder="กรอกนามสกุล" required>
  
  </div>
  </div>
  <div class="form-group">
    <label for="inputPhone_number">เบอร์โทร</label>
    <input type="text" class="form-control" id="Phone_Number"  name="Phone_Number" placeholder="กรอกเบอร์โทร" value="<?php echo htmlentities($result->Phone_Number); ?>" required>
  
  </div>
  <div class="form-group">
    <label for="inputPassword">Password</label>
    <input type="Password" class="form-control" id="Password"  minlength="8" name="Password" placeholder="กรอก Password"  >
  
  </div>
  <div class="col text-center">
  <button type="submit" class="btn btn-primary " name="apply" id="apply" onclick="return confirm('ยืนยันการเพิ่มสมาชิก')">บันทึกสมาชิก</button>
  </div>

    </div>

    <?php
    }
}
}
    ?>
  </article>
</section>


</body>
</html>

<?php
    }
?>