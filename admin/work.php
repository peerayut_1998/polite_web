<?php
session_start();
error_reporting(0);
include '../includes/config.php';
if(strlen($_SESSION['Usernameadmin'])==0)
    {  
      header('location:../index.php');
    }else{

?>


<!DOCTYPE html>
<html lang="en">
<head>
<title>รายงาน</title>
<style>

</style>
<meta charset="utf-8">
<link rel="stylesheet" href="../includes/css/styles.css">
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Kanit:wght@100;200;300;400;500&family=Roboto:ital,wght@0,100;0,400;0,500;0,700;0,900;1,500;1,700;1,900&display=swap" rel="stylesheet">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <script src="../includes/js/bootstrap.js"></script>  

<?php include '../includes/css/bootstrap5.php'?>

</head>
<body>

<?php include '../includes/admin/header.php'?>

<section>
<?php include '../includes/admin/sidebar.php'?>
  
  
  <article>
  <?php

$Username=$_GET['Username'];
$Week=$_GET['Week'];
$dayTH = array('Sunday'=>'อา','Monday'=>'จ','Tuesday'=>'อ','Wednesday'=>'พ','Thursday'=>'พฤ','Friday'=>'ศ','Saturday'=>'ส');
$listm = ["ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค."];
$Weeknext=$Week+1;
$Weekback=$Week-1;


?>

<h1 class="text-center bg-info"> สัปดาห์ที่ <?php echo $Week ?> </h1>
<?php
$sql = "SELECT * FROM polite_user WHERE Username=:Username";
$query = $dbh -> prepare($sql);
$query -> bindParam(':Username',$Username, PDO::PARAM_STR);
$query->execute();
$results=$query->fetchAll(PDO::FETCH_OBJ);

if($query->rowCount() > 0)
{
 
  foreach($results as $result)
  { 
      ?>
      <h4 class="text-danger">Username : <?php echo htmlentities($result->Username) ?> </h4>
      <h4 class="text-success">ชื่อ-นามสกุล : <?php echo htmlentities($result->First_Name) ?>  <?php echo htmlentities($result->Last_Name) ?></h4>
      <?php
  }
}
    
       ?>

<div class="container-xxl" style="background-color:#CC9999">
<div class="row align-items-start p-8">
<div class="col-5 p-3" style="font-size:18px "  >
แนวการปฎิบัติการรับประทานอาหาร
    </div>
    <?php
    $sql = "SELECT * from polite_user where Username=:Username";
    $query = $dbh -> prepare($sql);
    $query->bindParam(':Username',$Username,PDO::PARAM_STR);
    $query->execute();
    $results=$query->fetchAll(PDO::FETCH_OBJ);
  
    if($query->rowCount() > 0)
    {
      foreach($results as $result)
      {  

    ?>
    <div class="col-1 p-1 mt-2">
    <?php $application_date_str=$result->application_date;
      $time = strtotime($application_date_str);
      $newformat = date('Y-m-d',$time);
      $day=(($Week-1)*7)+0;
      $daystr=(string)$day;
      $NewDate=date( "Y-m-d-l", strtotime( "$newformat +"."$daystr"." day" ) ); 
      $sub_str = explode("-", $NewDate);
      $mint= (int)$sub_str[1];
      echo $dayTH[$sub_str[3]]." ".$sub_str[2]." ".$listm[$mint-1];
    ?>
    </div>
    <div class="col-1 p-1 mt-2">
    <?php $application_date_str=$result->application_date;
      $time = strtotime($application_date_str);
      $newformat = date('Y-m-d',$time);
      $day=(($Week-1)*7)+1;
      $daystr=(string)$day;
      $NewDate=date( "Y-m-d-l", strtotime( "$newformat +"."$daystr"." day" ) ); 
      $sub_str = explode("-", $NewDate);
      $mint= (int)$sub_str[1];
      echo $dayTH[$sub_str[3]]." ".$sub_str[2]." ".$listm[$mint-1];
    ?>
    </div>
    <div class="col-1 p-1 mt-2">
    <?php $application_date_str=$result->application_date;
      $time = strtotime($application_date_str);
      $newformat = date('Y-m-d',$time);
      $day=(($Week-1)*7)+2;
      $daystr=(string)$day;
      $NewDate=date( "Y-m-d-l", strtotime( "$newformat +"."$daystr"." day" ) ); 
      $sub_str = explode("-", $NewDate);
      $mint= (int)$sub_str[1];
      echo $dayTH[$sub_str[3]]." ".$sub_str[2]." ".$listm[$mint-1];
    ?>
    </div>
    <div class="col-1 p-1 mt-2">
    <?php $application_date_str=$result->application_date;
      $time = strtotime($application_date_str);
      $newformat = date('Y-m-d',$time);
      $day=(($Week-1)*7)+3;
      $daystr=(string)$day;
      $NewDate=date( "Y-m-d-l", strtotime( "$newformat +"."$daystr"." day" ) ); 
      $sub_str = explode("-", $NewDate);
      $mint= (int)$sub_str[1];
      echo $dayTH[$sub_str[3]]." ".$sub_str[2]." ".$listm[$mint-1];
    ?>
    </div>
    <div class="col-1 p-1 mt-2">
    <?php $application_date_str=$result->application_date;
      $time = strtotime($application_date_str);
      $newformat = date('Y-m-d',$time);
      $day=(($Week-1)*7)+4;
      $daystr=(string)$day;
      $NewDate=date( "Y-m-d-l", strtotime( "$newformat +"."$daystr"." day" ) ); 
      $sub_str = explode("-", $NewDate);
      $mint= (int)$sub_str[1];
      echo $dayTH[$sub_str[3]]." ".$sub_str[2]." ".$listm[$mint-1];
    ?>
    </div>
    <div class="col-1 p-1 mt-2">
    <?php $application_date_str=$result->application_date;
      $time = strtotime($application_date_str);
      $newformat = date('Y-m-d',$time);
      $day=(($Week-1)*7)+5;
      $daystr=(string)$day;
      $NewDate=date( "Y-m-d-l", strtotime( "$newformat +"."$daystr"." day" ) ); 
      $sub_str = explode("-", $NewDate);
      $mint= (int)$sub_str[1];
      echo $dayTH[$sub_str[3]]." ".$sub_str[2]." ".$listm[$mint-1];
    ?>
    </div>
    <div class="col-1 p-1 mt-2">
    <?php $application_date_str=$result->application_date;
      $time = strtotime($application_date_str);
      $newformat = date('Y-m-d',$time);
      $day=(($Week-1)*7)+6;
      $daystr=(string)$day;
      $NewDate=date( "Y-m-d-l", strtotime( "$newformat +"."$daystr"." day" ) ); 
      $sub_str = explode("-", $NewDate);
      $mint= (int)$sub_str[1];
      echo $dayTH[$sub_str[3]]." ".$sub_str[2]." ".$listm[$mint-1];
    ?>
    </div>
    <?php
      }
    }
    ?>
  </div>
  <hr size="5">
  
<?php


                         $sql = " SELECT * FROM polite_prescription WHERE Username=:Username AND Week=:Week";
                        $query = $dbh -> prepare($sql);
                        $query -> bindParam(':Username',$Username, PDO::PARAM_STR);
                        $query -> bindParam(':Week',$Week, PDO::PARAM_INT);
                        $query->execute();
                        $results=$query->fetchAll(PDO::FETCH_OBJ);
                        
                        if($query->rowCount() > 0)
                        {
                         
                          foreach($results as $result)
                          { 
                            
                               ?>

<?php 
  if($result->List=='011'){
    
    ?>
    <form method="post" >
<div class="row align-items-start p-1 ">
   
    <div class="col-5 mr-4"  style="font-size:16px">
    1. ลดอาหารกลุ่มคาร์โบไฮเดรต เช่น ข้าว ขนมปัง  ขนมหวาน
      </div>
      <div class="col-1">
    
      <input class="form-check-input" type="checkbox" name="mo1_1" id="mo1_1" <?php if($result->Monday=="1"){ ?> checked  value="15" <?php  } ?> >
      </div>
      <div class="col-1">
      <input class="form-check-input" type="checkbox" name="tu1_1" id="tu1_1" <?php if($result->Tuesday=="1"){ ?> checked <?php } ?>  >
      </div>
      <div class="col-1">
      <input class="form-check-input" type="checkbox" name="we1_1" id="we1_1" <?php if($result->Wednesday=="1"){ ?> checked <?php } ?> >
      </div>
      <div class="col-1">
      <input class="form-check-input" type="checkbox" name="th1_1" id="th1_1" <?php if($result->Thursday=="1"){ ?> checked <?php } ?> >
      </div>
      <div class="col-1">
      <input class="form-check-input" type="checkbox" name="fr1_1" id="fr1_1" <?php if($result->Friday=="1"){ ?> checked <?php } ?>>
      </div>
      <div class="col-1">
      <input class="form-check-input" type="checkbox" name="sa1_1" id="sa1_1" <?php if($result->Saturday=="1"){ ?> checked <?php } ?> >
      </div>
      <div class="col">
      <input class="form-check-input" type="checkbox" name="su1_1" id="su1_1"  <?php if($result->Sunday=="1"){ ?> checked <?php } ?> >
      </div>
    </div>
    <?php
  }
  if($result->List=='012'){
    
    ?>
<div class="row align-items-start p-1 ">
    
    <div class="col-5 mr-4"  style="font-size:16px">
    2. เพิ่มอาหารโปรตีนคุณภาพดี เช่น นม ไข่ต้ม ปลา
      </div>
      <div class="col-1">
      <input class="form-check-input" type="checkbox" name="mo1_2" id="mo1_2" <?php if($result->Monday=="1"){ ?> checked <?php } ?> >
      </div>
      <div class="col-1">
      <input class="form-check-input" type="checkbox" name="tu1_2" id="tu1_2" <?php if($result->Tuesday=="1"){ ?> checked <?php } ?>  >
      </div>
      <div class="col-1">
      <input class="form-check-input" type="checkbox" name="we1_2" id="we1_2" <?php if($result->Wednesday=="1"){ ?> checked <?php } ?> >
      </div>
      <div class="col-1">
      <input class="form-check-input" type="checkbox" name="th1_2" id="th1_2" <?php if($result->Thursday=="1"){ ?> checked <?php } ?> >
      </div>
      <div class="col-1">
      <input class="form-check-input" type="checkbox" name="fr1_2" id="fr1_2" <?php if($result->Friday=="1"){ ?> checked <?php } ?>>
      </div>
      <div class="col-1">
      <input class="form-check-input" type="checkbox" name="sa1_2" id="sa1_2" <?php if($result->Saturday=="1"){ ?> checked <?php } ?> >
      </div>
      <div class="col">
      <input class="form-check-input" type="checkbox" name="su1_2" id="su1_2"  <?php if($result->Sunday=="1"){ ?> checked <?php } ?> >
      </div>
    </div>
    <?php
  }
  if($result->List=='013'){
?>
<div class="row align-items-start p-1 ">
    
    <div class="col-5 mr-4"  style="font-size:16px">
    3. ไม่รับประทานอาหารเย็นหลัง 20.00 น
      </div>
      <div class="col-1">
      <input class="form-check-input" type="checkbox" name="mo1_3" id="mo1_3" <?php if($result->Monday=="1"){ ?> checked <?php } ?> >
      </div>
      <div class="col-1">
      <input class="form-check-input" type="checkbox" name="tu1_3" id="tu1_3" <?php if($result->Tuesday=="1"){ ?> checked <?php } ?>  >
      </div>
      <div class="col-1">
      <input class="form-check-input" type="checkbox" name="we1_3" id="we1_3" <?php if($result->Wednesday=="1"){ ?> checked <?php } ?> >
      </div>
      <div class="col-1">
      <input class="form-check-input" type="checkbox" name="th1_3" id="th1_3" <?php if($result->Thursday=="1"){ ?> checked <?php } ?> >
      </div>
      <div class="col-1">
      <input class="form-check-input" type="checkbox" name="fr1_3" id="fr1_3" <?php if($result->Friday=="1"){ ?> checked <?php } ?>>
      </div>
      <div class="col-1">
      <input class="form-check-input" type="checkbox" name="sa1_3" id="sa1_3" <?php if($result->Saturday=="1"){ ?> checked <?php } ?> >
      </div>
      <div class="col">
      <input class="form-check-input" type="checkbox" name="su1_3" id="su1_3"  <?php if($result->Sunday=="1"){ ?> checked <?php } ?> >
      </div>
    </div>
<?php
  }
  
  if($result->List=='014'){
    ?>

<div class="row align-items-start p-1 ">
    
    <div class="col-5 mr-4"  style="font-size:16px">
    4. กินข้าวเสร็จแล้วไม่กินขนมหวานต่อ หรือของกินเล่น
      </div>
      <div class="col-1">
      <input class="form-check-input" type="checkbox" name="mo1_4" id="mo1_4" <?php if($result->Monday=="1"){ ?> checked <?php } ?> >
      </div>
      <div class="col-1">
      <input class="form-check-input" type="checkbox" name="tu1_4" id="tu1_4" <?php if($result->Tuesday=="1"){ ?> checked <?php } ?>  >
      </div>
      <div class="col-1">
      <input class="form-check-input" type="checkbox" name="we1_4" id="we1_4" <?php if($result->Wednesday=="1"){ ?> checked <?php } ?> >
      </div>
      <div class="col-1">
      <input class="form-check-input" type="checkbox" name="th1_4" id="th1_4" <?php if($result->Thursday=="1"){ ?> checked <?php } ?> >
      </div>
      <div class="col-1">
      <input class="form-check-input" type="checkbox" name="fr1_4" id="fr1_4" <?php if($result->Friday=="1"){ ?> checked <?php } ?>>
      </div>
      <div class="col-1">
      <input class="form-check-input" type="checkbox" name="sa1_4" id="sa1_4" <?php if($result->Saturday=="1"){ ?> checked <?php } ?> >
      </div>
      <div class="col">
      <input class="form-check-input" type="checkbox" name="su1_4" id="su1_4"  <?php if($result->Sunday=="1"){ ?> checked <?php } ?> >
      </div>
    </div>
    <?php
  }
  if($result->List=='015'){
    ?>
<div class="row align-items-start p-1 ">
    
    <div class="col-5 mr-4"  style="font-size:16px">
    5. ดื่มน้ำเปล่าวันละ 2 ลิตร หรือ 8 - 10 แก้ว
      </div>
      <div class="col-1">
      <input class="form-check-input" type="checkbox" name="mo1_5" id="mo1_5" <?php if($result->Monday=="1"){ ?> checked <?php } ?> >
      </div>
      <div class="col-1">
      <input class="form-check-input" type="checkbox" name="tu1_5" id="tu1_5" <?php if($result->Tuesday=="1"){ ?> checked <?php } ?>  >
      </div>
      <div class="col-1">
      <input class="form-check-input" type="checkbox" name="we1_5" id="we1_5" <?php if($result->Wednesday=="1"){ ?> checked <?php } ?> >
      </div>
      <div class="col-1">
      <input class="form-check-input" type="checkbox" name="th1_5" id="th1_5" <?php if($result->Thursday=="1"){ ?> checked <?php } ?> >
      </div>
      <div class="col-1">
      <input class="form-check-input" type="checkbox" name="fr1_5" id="fr1_5" <?php if($result->Friday=="1"){ ?> checked <?php } ?>>
      </div>
      <div class="col-1">
      <input class="form-check-input" type="checkbox" name="sa1_5" id="sa1_5" <?php if($result->Saturday=="1"){ ?> checked <?php } ?> >
      </div>
      <div class="col">
      <input class="form-check-input" type="checkbox" name="su1_5" id="su1_5"  <?php if($result->Sunday=="1"){ ?> checked <?php } ?> >
      </div>
    </div>
    <?php
  }
  if($result->List=='016'){
    ?>
<div class="row align-items-start p-1 ">
    
    <div class="col-5 mr-4"  style="font-size:16px">
    6. เน้นอาหารต้ม นึ่ง ลดอาหาร ทอด หวาน มัน  เค็ม
      </div>
      <div class="col-1">
      <input class="form-check-input" type="checkbox" name="mo1_6" id="mo1_6" <?php if($result->Monday=="1"){ ?> checked <?php } ?> >
      </div>
      <div class="col-1">
      <input class="form-check-input" type="checkbox" name="tu1_6" id="tu1_6" <?php if($result->Tuesday=="1"){ ?> checked <?php } ?>  >
      </div>
      <div class="col-1">
      <input class="form-check-input" type="checkbox" name="we1_6" id="we1_6" <?php if($result->Wednesday=="1"){ ?> checked <?php } ?> >
      </div>
      <div class="col-1">
      <input class="form-check-input" type="checkbox" name="th1_6" id="th1_6" <?php if($result->Thursday=="1"){ ?> checked <?php } ?> >
      </div>
      <div class="col-1">
      <input class="form-check-input" type="checkbox" name="fr1_6" id="fr1_6" <?php if($result->Friday=="1"){ ?> checked <?php } ?>>
      </div>
      <div class="col-1">
      <input class="form-check-input" type="checkbox" name="sa1_6" id="sa1_6" <?php if($result->Saturday=="1"){ ?> checked <?php } ?> >
      </div>
      <div class="col">
      <input class="form-check-input" type="checkbox" name="su1_6" id="su1_6"  <?php if($result->Sunday=="1"){ ?> checked <?php } ?> >
      </div>
    </div>
    <?php
  }
  if($result->List=='021'){
    ?>


<hr size="5">
  <div class="row align-items-start p-1">
<div class="col-5" style="font-size:18px"  >
แนวการปฎิบัติการรับประทานอาหาร
    </div>
    <?php
    $sql = "SELECT * from polite_user where Username=:Username";
    $query = $dbh -> prepare($sql);
    $query->bindParam(':Username',$Username,PDO::PARAM_STR);
    $query->execute();
    $results=$query->fetchAll(PDO::FETCH_OBJ);
  
    if($query->rowCount() > 0)
    {
      foreach($results as $result)
      {  

    ?>
    <div class="col-1 p-1 mt-2">
    <?php $application_date_str=$result->application_date;
      $time = strtotime($application_date_str);
      $newformat = date('Y-m-d',$time);
      $day=(($Week-1)*7)+0;
      $daystr=(string)$day;
      $NewDate=date( "Y-m-d-l", strtotime( "$newformat +"."$daystr"." day" ) ); 
      $sub_str = explode("-", $NewDate);
      $mint= (int)$sub_str[1];
      echo $dayTH[$sub_str[3]]." ".$sub_str[2]." ".$listm[$mint-1];
    ?>
    </div>
    <div class="col-1 p-1 mt-2">
    <?php $application_date_str=$result->application_date;
      $time = strtotime($application_date_str);
      $newformat = date('Y-m-d',$time);
      $day=(($Week-1)*7)+1;
      $daystr=(string)$day;
      $NewDate=date( "Y-m-d-l", strtotime( "$newformat +"."$daystr"." day" ) ); 
      $sub_str = explode("-", $NewDate);
      $mint= (int)$sub_str[1];
      echo $dayTH[$sub_str[3]]." ".$sub_str[2]." ".$listm[$mint-1];
    ?>
    </div>
    <div class="col-1 p-1 mt-2">
    <?php $application_date_str=$result->application_date;
      $time = strtotime($application_date_str);
      $newformat = date('Y-m-d',$time);
      $day=(($Week-1)*7)+2;
      $daystr=(string)$day;
      $NewDate=date( "Y-m-d-l", strtotime( "$newformat +"."$daystr"." day" ) ); 
      $sub_str = explode("-", $NewDate);
      $mint= (int)$sub_str[1];
      echo $dayTH[$sub_str[3]]." ".$sub_str[2]." ".$listm[$mint-1];
    ?>
    </div>
    <div class="col-1 p-1 mt-2">
    <?php $application_date_str=$result->application_date;
      $time = strtotime($application_date_str);
      $newformat = date('Y-m-d',$time);
      $day=(($Week-1)*7)+3;
      $daystr=(string)$day;
      $NewDate=date( "Y-m-d-l", strtotime( "$newformat +"."$daystr"." day" ) ); 
      $sub_str = explode("-", $NewDate);
      $mint= (int)$sub_str[1];
      echo $dayTH[$sub_str[3]]." ".$sub_str[2]." ".$listm[$mint-1];
    ?>
    </div>
    <div class="col-1 p-1 mt-2">
    <?php $application_date_str=$result->application_date;
      $time = strtotime($application_date_str);
      $newformat = date('Y-m-d',$time);
      $day=(($Week-1)*7)+4;
      $daystr=(string)$day;
      $NewDate=date( "Y-m-d-l", strtotime( "$newformat +"."$daystr"." day" ) ); 
      $sub_str = explode("-", $NewDate);
      $mint= (int)$sub_str[1];
      echo $dayTH[$sub_str[3]]." ".$sub_str[2]." ".$listm[$mint-1];
    ?>
    </div>
    <div class="col-1 p-1 mt-2">
    <?php $application_date_str=$result->application_date;
      $time = strtotime($application_date_str);
      $newformat = date('Y-m-d',$time);
      $day=(($Week-1)*7)+5;
      $daystr=(string)$day;
      $NewDate=date( "Y-m-d-l", strtotime( "$newformat +"."$daystr"." day" ) ); 
      $sub_str = explode("-", $NewDate);
      $mint= (int)$sub_str[1];
      echo $dayTH[$sub_str[3]]." ".$sub_str[2]." ".$listm[$mint-1];
    ?>
    </div>
    <div class="col-1 p-1 mt-2">
    <?php $application_date_str=$result->application_date;
      $time = strtotime($application_date_str);
      $newformat = date('Y-m-d',$time);
      $day=(($Week-1)*7)+6;
      $daystr=(string)$day;
      $NewDate=date( "Y-m-d-l", strtotime( "$newformat +"."$daystr"." day" ) ); 
      $sub_str = explode("-", $NewDate);
      $mint= (int)$sub_str[1];
      echo $dayTH[$sub_str[3]]." ".$sub_str[2]." ".$listm[$mint-1];
    ?>
    </div>
    <?php
      }
    }
    ?>
  </div>
  <hr size="5">
  <div class="row align-items-start p-1 ">
    
    <div class="col-5 mr-4"  style="font-size:16px">
    1. ออกกำลังกายได้ 20 - 30 นาที ต่อครั้ง
      </div>
      <div class="col-1">
      <input class="form-check-input" type="checkbox" name="mo2_1" id="mo2_1" <?php if($result->Monday=="1"){ ?> checked <?php } ?> >
      </div>
      <div class="col-1">
      <input class="form-check-input" type="checkbox" name="tu2_1" id="tu2_1" <?php if($result->Tuesday=="1"){ ?> checked <?php } ?>  >
      </div>
      <div class="col-1">
      <input class="form-check-input" type="checkbox" name="we2_1" id="we2_1" <?php if($result->Wednesday=="1"){ ?> checked <?php } ?> >
      </div>
      <div class="col-1">
      <input class="form-check-input" type="checkbox" name="th2_1" id="th2_1" <?php if($result->Thursday=="1"){ ?> checked <?php } ?> >
      </div>
      <div class="col-1">
      <input class="form-check-input" type="checkbox" name="fr2_1" id="fr2_1" <?php if($result->Friday=="1"){ ?> checked <?php } ?>>
      </div>
      <div class="col-1">
      <input class="form-check-input" type="checkbox" name="sa2_1" id="sa2_1" <?php if($result->Saturday=="1"){ ?> checked <?php } ?> >
      </div>
      <div class="col">
      <input class="form-check-input" type="checkbox" name="su2_1" id="su2_1"  <?php if($result->Sunday=="1"){ ?> checked <?php } ?> >
      </div>
    </div>
    <?php
  }
if($result->List=='022'){
?>
<div class="row align-items-start p-1 ">
    
    <div class="col-5 mr-4"  style="font-size:16px">
    2. ลักษณะออดกำลังกายมีเหงื่อซึม และหายใจเร็วกว่าปกติ
      </div>
      <div class="col-1">
      <input class="form-check-input" type="checkbox" name="mo2_2" id="mo2_2" <?php if($result->Monday=="1"){ ?> checked <?php } ?> >
      </div>
      <div class="col-1">
      <input class="form-check-input" type="checkbox" name="tu2_2" id="tu2_2" <?php if($result->Tuesday=="1"){ ?> checked <?php } ?>  >
      </div>
      <div class="col-1">
      <input class="form-check-input" type="checkbox" name="we2_2" id="we2_2" <?php if($result->Wednesday=="1"){ ?> checked <?php } ?> >
      </div>
      <div class="col-1">
      <input class="form-check-input" type="checkbox" name="th2_2" id="th2_2" <?php if($result->Thursday=="1"){ ?> checked <?php } ?> >
      </div>
      <div class="col-1">
      <input class="form-check-input" type="checkbox" name="fr2_2" id="fr2_2" <?php if($result->Friday=="1"){ ?> checked <?php } ?>>
      </div>
      <div class="col-1">
      <input class="form-check-input" type="checkbox" name="sa2_2" id="sa2_2" <?php if($result->Saturday=="1"){ ?> checked <?php } ?> >
      </div>
      <div class="col">
      <input class="form-check-input" type="checkbox" name="su2_2" id="su2_2"  <?php if($result->Sunday=="1"){ ?> checked <?php } ?> >
      </div>
    </div>
<?php
}
if($result->List=='023'){
  ?>
<div class="row align-items-start p-1 ">
    
    <div class="col-5 mr-4"  style="font-size:16px">
    3. ออกกำลังกายท่าบริหารกระชับเอว พุง
      </div>
      <div class="col-1">
      <input class="form-check-input" type="checkbox" name="mo2_3" id="mo2_3" <?php if($result->Monday=="1"){ ?> checked <?php } ?> >
      </div>
      <div class="col-1">
      <input class="form-check-input" type="checkbox" name="tu2_3" id="tu2_3" <?php if($result->Tuesday=="1"){ ?> checked <?php } ?>  >
      </div>
      <div class="col-1">
      <input class="form-check-input" type="checkbox" name="we2_3" id="we2_3" <?php if($result->Wednesday=="1"){ ?> checked <?php } ?> >
      </div>
      <div class="col-1">
      <input class="form-check-input" type="checkbox" name="th2_3" id="th2_3" <?php if($result->Thursday=="1"){ ?> checked <?php } ?> >
      </div>
      <div class="col-1">
      <input class="form-check-input" type="checkbox" name="fr2_3" id="fr2_3" <?php if($result->Friday=="1"){ ?> checked <?php } ?>>
      </div>
      <div class="col-1">
      <input class="form-check-input" type="checkbox" name="sa2_3" id="sa2_3" <?php if($result->Saturday=="1"){ ?> checked <?php } ?> >
      </div>
      <div class="col">
      <input class="form-check-input" type="checkbox" name="su2_3" id="su2_3"  <?php if($result->Sunday=="1"){ ?> checked <?php } ?> >
      </div>
    </div>
    <br>
  <?php
}

  ?>                     
  
  <?php
                          }}
  ?>
  

  </div>
  <br>
<br>

<div class="container-xxl">
    <div style="background-color:#CC9999"><label for="Textarea"><p style="margin: 10px 10px; font-size:18px">ปัญหาอุปสรรค ในการเปลี่ยนแปลงของคุณ</p></label>
  <?php


                       $sql = "SELECT * FROM polite_obstacle WHERE Username=:Username AND Week=:Week";
                      $query = $dbh -> prepare($sql);
                      $query -> bindParam(':Username',$Username, PDO::PARAM_STR);
                      $query -> bindParam(':Week',$Week, PDO::PARAM_INT);
                      $query->execute();
                      $results=$query->fetchAll(PDO::FETCH_OBJ);
                      
                      if($query->rowCount() > 0)
                      {
                       
                        foreach($results as $result)
                        { 
                             ?>
  <textarea class="form-control" id="Textarea" name="Textarea" rows="3"><?php echo htmlentities($result->details)?></textarea>

  <?php
}
}
  ?>
</div>
<div class="container" style="padding-top:10px ;">
<div class="row">
    <?php if($Weekback!=0){
?>
    
  <div class="col-sm text-start">
   <a style="cursor: pointer;" href="<?php echo "work.php?Username=".$Username."&Week=".$Weekback;?>" > <img src="img/main/left-arrow.png" alt="ย้อนกลับ" width="75px"></a>
  </div>
  <?php }?>
  <?php if($Weeknext!=13){
      ?>
  <div class="col-sm text-end">
  <a style="cursor: pointer;" href="<?php  echo "work.php?Username=".$Username."&Week=".$Weeknext;?>"><img src="img/main/right-arrow.png" alt="ถัดไป" width="75px"></a>
  </div>
  <?php
  }
  ?>

</div>
</div>
</form>


</div>




</div>

  </article>
</section>


</body>
</html>
<?php

}
?>