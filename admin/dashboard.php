<?php
session_start();
error_reporting(0);
include '../includes/config.php';
if(strlen($_SESSION['Usernameadmin'])==0)
    {  
    header('location:../index.php');
    }else{


?>


<!DOCTYPE html>
<html lang="en">
<head>
<title>แก้ไขข้อมูลสมัครชิก</title>
<meta charset="utf-8">
<link rel="stylesheet" href="../includes/css/styles.css">
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Kanit:wght@100;200;300;400;500&family=Roboto:ital,wght@0,100;0,400;0,500;0,700;0,900;1,500;1,700;1,900&display=swap" rel="stylesheet">
<meta name="viewport" content="width=device-width, initial-scale=1">
<?php include '../includes/css/bootstrap5.php'?>
<?php

   $dataq[10];
   $datay[10];
   $datan[10];
   $Username=$_SESSION['Username'];
$sql = "SELECT SUM(t1='q') as t1q, SUM(t1='y') as t1y,SUM(t1='n') as t1n,SUM(t2='q') as t2q, SUM(t2='y') as t2y,SUM(t2='n') as t2n,
SUM(t3='q') as t3q, SUM(t3='y') as t3y,SUM(t3='n') as t3n,
SUM(t4='q') as t4q, SUM(t1='y') as t4y,SUM(t4='n') as t4n,
SUM(t5='q') as t5q, SUM(t5='y') as t5y,SUM(t5='n') as t5n,
SUM(t6='q') as t6q, SUM(t6='y') as t6y,SUM(t6='n') as t6n,
SUM(t7='q') as t7q, SUM(t7='y') as t7y,SUM(t1='n') as t7n,
SUM(t8='q') as t8q, SUM(t8='y') as t8y,SUM(t8='n') as t8n,
SUM(t9='q') as t9q, SUM(t9='y') as t9y,SUM(t9='n') as t9n
FROM `polite_prescription_new` WHERE Username=:Username";
$query = $dbh -> prepare($sql);
$query->bindParam(':Username',$Username,PDO::PARAM_STR);
$query->execute();
$results=$query->fetchAll(PDO::FETCH_OBJ);

if($query->rowCount() > 0)
{
 foreach($results as $result)
 {
   
   $dataq[1] = $result->t1q;
   $dataq[2] = $result->t2q;
   $dataq[3] = $result->t3q;
   $dataq[4] = $result->t4q;
   $dataq[5] = $result->t5q;
   $dataq[6] = $result->t6q;
   $dataq[7] = $result->t7q;
   $dataq[8] = $result->t8q;
   $dataq[9] = $result->t9q;
   $datay[1] = $result->t1y;
   $datay[2] = $result->t2y;
   $datay[3] = $result->t3y;
   $datay[4] = $result->t4y;
   $datay[5] = $result->t5y;
   $datay[6] = $result->t6y;
   $datay[7] = $result->t7y;
   $datay[8] = $result->t8y;
   $datay[9] = $result->t9y;
   $datan[1] = $result->t1n;
   $datan[2] = $result->t2n;
   $datan[3] = $result->t3n;
   $datan[4] = $result->t4n;
   $datan[5] = $result->t5n;
   $datan[6] = $result->t6n;
   $datan[7] = $result->t7n;
   $datan[8] = $result->t8n;
   $datan[9] = $result->t9n;
   
 }
 }
$dataPoints1 = array(
  array("label"=> "1", "y"=> $dataq[1]),
  array("label"=> "2", "y"=> $dataq[2]),
  array("label"=> "3", "y"=> $dataq[3]),
  array("label"=> "4", "y"=> $dataq[4]),
  array("label"=> "5", "y"=> $dataq[5]),
  array("label"=> "6", "y"=> $dataq[6]),
  array("label"=> "7", "y"=> $dataq[7]),
  array("label"=> "8", "y"=> $dataq[8]),
  array("label"=> "9", "y"=> $dataq[9]),
);
 
$dataPoints2 = array(
  array("label"=> "1", "y"=> $datay[1]),
  array("label"=> "2", "y"=> $datay[2]),
  array("label"=> "3", "y"=> $datay[3]),
  array("label"=> "4", "y"=> $datay[4]),
  array("label"=> "5", "y"=> $datay[5]),
  array("label"=> "6", "y"=> $datay[6]),
  array("label"=> "7", "y"=> $datay[7]),
  array("label"=> "8", "y"=> $datay[8]),
  array("label"=> "9", "y"=> $datay[9]),
);
 
$dataPoints3 = array(
 array("label"=> "1", "y"=> $datan[1]),
 array("label"=> "2", "y"=> $datan[2]),
 array("label"=> "3", "y"=> $datan[3]),
 array("label"=> "4", "y"=> $datan[4]),
 array("label"=> "5", "y"=> $datan[5]),
 array("label"=> "6", "y"=> $datan[6]),
 array("label"=> "7", "y"=> $datan[7]),
 array("label"=> "8", "y"=> $datan[8]),
 array("label"=> "9", "y"=> $datan[9]),
);
  
 
  
 ?>
 <!DOCTYPE HTML>
 <html>
 <head>  
 <script>
 window.onload = function () {
  
 var chart = new CanvasJS.Chart("chartContainer", {
   title: {
     text: "สรุปผลกิจกรรม"
   },
   theme: "light1",
   animationEnabled: true,
   toolTip:{
     shared: true,
    //  reversed: true
   },
   axisY: {
     suffix: "%"
   },
   data: [
     {
       type: "stackedColumn100",
       name: "ทำ",
       showInLegend: true,
       yValueFormatString: "#,##0 วัน",
       dataPoints: <?php echo json_encode($dataPoints3, JSON_NUMERIC_CHECK); ?>
     },{
       type: "stackedColumn100",
       name: "ไม่ทำ",
       showInLegend: true,
       yValueFormatString: "#,##0 วัน",
       dataPoints: <?php echo json_encode($dataPoints2, JSON_NUMERIC_CHECK); ?>
     },{
       type: "stackedColumn100",
       name: "รอ",
       showInLegend: true,
       yValueFormatString: "#,##0 วัน",
       dataPoints: <?php echo json_encode($dataPoints1, JSON_NUMERIC_CHECK); ?>
     }
   ]
 });
  
 chart.render();
  
 }
 </script>
</head>
<body>
<?php include '../includes/admin/header.php'?>
<section>
<?php include '../includes/admin/sidebar.php'?>


<article>

<div class="container">
      <br>
    <div id="chartContainer" style="height: 500px; width: 100%; "  class="mb-5"></div>
<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
<table class="table">
  <caption>เปรียบเทียบ</caption>
  <thead>
    <tr>
      <th scope="col">ข้อ</th>
      <th scope="col">เรื่อง</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">1</th>
      <td>ลดอาหารกลุ่มคาร์โบไฮเดรต เช่น ข้าว ขนมปัง ขนมหวาน</td>

    </tr>
    <tr>
      <th scope="row">2</th>
      <td>เพิ่มอาหารโปรตีนคุณภาพดี เช่น นม ไข่ต้ม ปลา</td>

    </tr>
    <tr>
      <th scope="row">3</th>
      <td>ไม่รับประทานอาหารเย็นหลัง 20.00 น</td>
    </tr>
    <tr>
      <th scope="row">4</th>
      <td>กินข้าวเสร็จแล้วไม่กินขนมหวานต่อ หรือของกินเล่น</td>
    </tr>
    <tr>
      <th scope="row">5</th>
      <td>ดื่มน้ำเปล่าวันละ 2 ลิตร หรือ 8 - 10 แก้ว</td>
    </tr>
    <tr>
      <th scope="row">6</th>
      <td>เน้นอาหารต้ม นึ่ง ลดอาหาร ทอด หวาน มัน เค็ม</td>
    </tr>
    <tr>
      <th scope="row">7</th>
      <td>ออกกำลังกายได้ 20 - 30 นาที ต่อครั้ง</td>
    </tr>
    <tr>
      <th scope="row">8</th>
      <td>ลักษณะออดกำลังกายมีเหงื่อซึม และหายใจเร็วกว่าปกติ</td>
    </tr>
    <tr>
      <th scope="row">9</th>
      <td>ออกกำลังกายท่าบริหารกระชับเอว พุง</td>
    </tr>
  </tbody>
</table>
    </div>


    
</div>
  </article>
</section>


</body>
</html>

<?php
    }
?>