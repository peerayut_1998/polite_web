
<?php
session_start();
error_reporting(0);
include 'includes/config.php';
if(strlen($_SESSION['Usernameadmin'])==0)
    {  
      header('location:../index.php');
    }else{

    if(isset($_POST['submit'])){
      if($_POST['New_Password']==$_POST['Confirm_Password']){
        $Username = $_SESSION['Username'];
        $Old_Password=md5($_POST['Old_Password']);
        $New_Password=md5($_POST['New_Password']);
        $Confirm_Password=md5($_POST['Confirm_Password']);
        $sql="UPDATE polite_admin SET Password=:New_Password WHERE Username=:Username";
        $query = $dbh -> prepare($sql);
        $query->bindParam(':New_Password',$New_Password,PDO::PARAM_STR);
        $query->bindParam(':Username',$Username,PDO::PARAM_STR);
        $query->execute();

      if($query->rowCount() > 0){
        echo "<script>alert('เปลี่ยนรหัสผ่านเรียบร้อย')</script>";
      }else{
        echo "<script>alert('ERROR')</script>";
      }
      }else{
        echo "<script>alert('รหัสผ่านกับยืนยันรหัสผ่านไม่ตรงกัน')</script>";
      }
       
    }

?>

<!DOCTYPE html>
<html lang="en">
<head>
<title>เปลี่ยนรหัสผ่าน</title>
<meta charset="utf-8">
<link rel="stylesheet" href="../includes/css/styles.css">
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Kanit:wght@100;200;300;400;500&family=Roboto:ital,wght@0,100;0,400;0,500;0,700;0,900;1,500;1,700;1,900&display=swap" rel="stylesheet">
<meta name="viewport" content="width=device-width, initial-scale=1">
<?php include '../includes/css/bootstrap5.php'?>
</head>
<body>
<?php include '../includes/admin/header.php'?>
<section>
<?php include '../includes/admin/sidebar.php'?>
<article>

<br>

<div class="container" style="font-size: 20px; padding:50px;">
<form method="post">
  <div class="form-group row">
    <label for="staticEmail" class="col-sm-2 col-form-label text-danger">Username</label>
    <div class="col-sm-10">
      <input type="text" readonly class="form-control-plaintext text-danger" id="Username" name="Username" value="<?php echo $_SESSION['Usernameadmin'] ?>" require>
    </div>
  </div>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label ">Old Password</label>
    <div class="col-sm-10">
      <input type="password" class="form-control" id="Old_Password" name="Old_Password"  minlength="8" placeholder="กรอกรหัสผ่านเก่าของท่าน" require>
    </div>
  </div>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">New Password</label>
    <div class="col-sm-10">
      <input type="password" class="form-control" id="New_Password" name="New_Password" minlength="8" placeholder="กรอกรหัสผ่าน" require>
    </div>
  </div>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">ConfirmPassword</label>
    <div class="col-sm-10">
      <input type="password" class="form-control" id="Confirm_Password" minlength="8" name="Confirm_Password" placeholder="ยืนยันรหัสผ่าน" require>
    </div>
  </div>
  <div class="text-center">
  <button type="submit" name="submit" class="btn-lg btn-info me-2">ยืนยันการเปลี่ยนรหัสผ่าน</button>
    </div>
</form>

  

<div>
  <br>
</body>
<section>
<footer>
<?php include 'includes/footer.php'?>
</footer>

</html>
<?php }?>