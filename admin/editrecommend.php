

<?php
session_start();
error_reporting(0);
include '../includes/config.php';
if(strlen($_SESSION['Usernameadmin'])==0)
    {  
      header('location:../index.php');
    }else{
if(isset($_POST['editdata']))
{
  $Id=$_GET['edit'];
 $Name=$_POST['Name'];
 


 if ( isset( $_FILES["gg"] ) && !empty( $_FILES["gg"]["name"] ) ){
    $name_file =  $_FILES['gg']['name'];
$name_type =  explode(".", $name_file)[1];
// generator img
function random_string($length) {
  $key = '';
  $keys = array_merge(range(0, 9), range('a', 'z'));

  for ($i = 0; $i < $length; $i++) {
      $key .= $keys[array_rand($keys)];
  }
  return $key;
}

$gen=random_string(50);
$name_file_save="$gen.$name_type";
$tmp_name =  $_FILES['gg']['tmp_name'];
$locate_img ="img/";
move_uploaded_file($tmp_name,$locate_img.$name_file_save);

$sql = "UPDATE polite_recommend SET Name=:Name,Image_file=:Image_file,Datetime=now() WHERE Id=:Id";
$query = $dbh->prepare($sql);
$query -> bindParam(':Name',$Name, PDO::PARAM_STR);
$query -> bindParam(':Id',$Id, PDO::PARAM_STR);
$query -> bindParam(':Image_file',$name_file_save, PDO::PARAM_STR);
$query -> execute();
$msg="แก้ไขเสร็จสิ้น";

  }else{
  
    $sql = "UPDATE polite_recommend SET Name=:Name,Datetime=now() WHERE Id=:Id";
    $query = $dbh->prepare($sql);
    $query -> bindParam(':Id',$Id, PDO::PARAM_STR);
    $query -> bindParam(':Name',$Name, PDO::PARAM_STR);
    
    $query -> execute();
  
   $msg="แก้ไขเสร็จสิ้น";
  }
 
 

}
?>

<!DOCTYPE html>
<html lang="en">
<head>
<title>แก้ไของค์ความรู้</title>
<meta charset="utf-8">
<link rel="stylesheet" href="../includes/css/styles.css">
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Kanit:wght@100;200;300;400;500&family=Roboto:ital,wght@0,100;0,400;0,500;0,700;0,900;1,500;1,700;1,900&display=swap" rel="stylesheet">
<meta name="viewport" content="width=device-width, initial-scale=1">
<?php include '../includes/css/bootstrap5.php'?>

</head>
<body>

<?php include '../includes/admin/header.php'?>

<section>
<?php include '../includes/admin/sidebar.php'?>

<?php 
if(isset($_GET['edit'])){
 
$Id=$_GET['edit'];
$sql = "SELECT * from polite_recommend WHERE Id=:Id";
$query = $dbh -> prepare($sql);
$query -> bindParam(':Id',$Id, PDO::PARAM_STR);
$query->execute();
$results=$query->fetchAll(PDO::FETCH_OBJ);
$cnt=1;
if($query->rowCount() > 0)
{
  foreach($results as $result)
{
  ?>
<article>
    <h1>แก้ไขข้อแนะนำ</h1>
    <hr>
    <div>
    <div>
    <form method="post" enctype=multipart/form-data>
  <div class="form-group">
    <label for="inputhead">ชื่อข้อแนะนำ</label>
    <input type="text" class="form-control" id="inputhead" name="Name" placeholder="กรอกชื่อข้อแนะนำ" value="<?php echo htmlentities($result->Name);?>"required>
  
  </div>
  <div class="mb-3">
  <label for="FormControlFile" class="form-label">รูปปกเก่า</label>
  <img src="img/<?php echo htmlentities($result->Image_file);?>" alt="<?php echo htmlentities($result->Image_file);?>" class="img-thumbnail " width="200" high="200">
  </div>
  <div class="mb-3">
  <label for="FormControlFile" class="form-label">เปลี่ยนรูป</label>
  <input type="file" name="gg" class="form-control" id="FormControlFile" accept="image/png, image/jpeg"  >
  </div>
 
  
  <div class="col text-center">
  <div class="modal-footer">
        
        <button type="submit" class="btn btn-warning "  name="editdata" id="editdata" onclick="alert('แก้ไขข้อมูลเสร็จสิ้น')">แก้ไข</button>
      </div>
  </div>
</form>
   
  </article>
</section>
<?php }}} ?>

</body>
</html>
<?php }?>
