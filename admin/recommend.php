<?php
session_start();
error_reporting(0);
include '../includes/config.php';
if(strlen($_SESSION['Usernameadmin'])==0)
    {  
      header('location:../index.php');
    }else{

if(isset($_GET['delete']))
{
  
$id=$_GET['delete'];
$sql = "delete from  polite_recommend  WHERE Id=:id";
$query = $dbh->prepare($sql);
$query -> bindParam(':id',$id, PDO::PARAM_STR);
$query -> execute();
$msg="ลบข้อมูลเสร็จสิ้น";
  
}
if(isset($_POST['apply']))
{

$Section=$_POST['name'];
$name_file =  $_FILES['gg']['name'];
$name_type =  explode(".", $name_file)[1];
// generator img
function random_string($length) {
  $key = '';
  $keys = array_merge(range(0, 9), range('a', 'z'));

  for ($i = 0; $i < $length; $i++) {
      $key .= $keys[array_rand($keys)];
  }

  return $key;
}
$gen=random_string(50);

$name_file_save="$gen.$name_type";

$tmp_name =  $_FILES['gg']['tmp_name'];
$locate_img ="img/";
move_uploaded_file($tmp_name,$locate_img.$name_file_save);

$sql="INSERT INTO polite_recommend (Name, Image_file, Datetime) VALUES (:Name,:namefile, now());";
$query = $dbh->prepare($sql);
$query->bindParam(':Name',$Section,PDO::PARAM_STR);
$query->bindParam(':namefile',$name_file_save,PDO::PARAM_STR);


$query->execute();
$lastInsertId = $dbh->lastInsertId();
if($lastInsertId)
{


$msg="recommend applied successfully";
}
else 
{

  echo $lastInsertId;
$error="Something went wrong. Please try again";
}
}


?>


<!DOCTYPE html>
<html lang="en">
<head>
<title>จัดการข้อแนะนำ</title>
<style>

</style>
<meta charset="utf-8">
<link rel="stylesheet" href="../includes/css/styles.css">
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Kanit:wght@100;200;300;400;500&family=Roboto:ital,wght@0,100;0,400;0,500;0,700;0,900;1,500;1,700;1,900&display=swap" rel="stylesheet">
<meta name="viewport" content="width=device-width, initial-scale=1">

<?php include '../includes/css/bootstrap5.php'?>

</head>
<body>

<?php include '../includes/admin/header.php'?>

<section>
<?php include '../includes/admin/sidebar.php'?>
  
  
  <article>

</form>
    </div>
    <h1>จัดการข้อแนะนำ</h1>
    <hr>
    <p>กดที่ปุ่มสีเขียนเพื่อเพิ่มข้อแนะนำ</p>

<!-- model add -->
    <button type="button" class="btn btn-success" data-toggle="modal" data-target=".bd-add-modal-lg">เพิ่มข้อแนะนำ</button>

<div class="modal fade bd-add-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    <div class="modal-header">
        <h5 class="modal-title" id="ModalLabel">เพิ่มข้อแนะนำ</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div>
    <form method="post" enctype=multipart/form-data>
  <div class="form-group">
    <label for="inputhead">ชื่อข้อแนะนำ</label>
    <input type="text" class="form-control" id="inputhead" name="name" placeholder="กรอกชื่อข้อแนะนำ" required>
  
  </div>
  <div class="mb-3">
  <label for="FormControlFile" class="form-label">ไฟล์ภาพหน้าปก</label>
  <input type="file" name="gg" class="form-control" id="FormControlFile" accept="image/png, image/jpeg" required>
  </div>
  
  <div class="col text-center">
  <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">ยกเลิก</button>
        <button type="submit" class="btn btn-primary "  name="apply" id="apply">บันทึก</button>
      </div>
  </div>
</form>
    </div>
      </div>
      
    </div>
    </div>
  </div>
</div>


  
    <br>
    <br>
 
  <?php $sql = "SELECT * from polite_recommend";
$query = $dbh -> prepare($sql);
$query->execute();
$results=$query->fetchAll(PDO::FETCH_OBJ);
$cnt=1;
if($query->rowCount() > 0)
{
  ?>
  <div>
      
  <table class="table table-striped table-bordered">
<thead>
  <tr>
    <th scope="col">ลำดับ</th>
    <th scope="col">ภาพปก</th>
    <th scope="col">ชื่อข้อแนะนำ</th>
    <th scope="col">แก้ไข</th>
    <th scope="col">ลบ</th>
  </tr>
</thead>
<tbody>
  <?php 
foreach($results as $result)
{               ?>  

<tr>
                                            <th scope="row"> <?php echo htmlentities($cnt);?></th>
                                            <td><img src="img/<?php echo htmlentities($result->Image_file);?>" alt="..." width="100" height="100"></td>
                                            <td style="word-break:break-all"><?php echo htmlentities($result->Name);?></td>
                                            <td><a href="editrecommend.php?edit=<?php echo htmlentities($result->Id);?>"><button type="button" class="btn btn-warning">แก้ไข</button></a></td>
                                            <td> <a href="recommend.php?delete=<?php echo htmlentities($result->Id);?>" onclick="return confirm('Do you want to delete');"><button type="button" class="btn btn-danger">ลบ</button></a></td>
                                           
                                           
                                            
                                        </tr>
                                         <?php $cnt++;}
                                        
                                        }  else {
                                        
                                          ?>
                                         <h1>ยังไม่ได้เพิ่มข้อมูล</h1>
                                          <?php
                                        }?>


   
  </tbody>
</table>


<!-- Modal delete-->
<div class="modal fade" id="delete-file-modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">

            <form class="form-horizontal" method="post" id="delete_file_form">


                <div class="modal-body">

                   ท่ายจะยื่นยันการลบหัวข้อแนะนำนี้หรือไหม

                </div>  

                <div class="modal-footer">

                    <button type="submit" data-dismiss="modal" class="btn btn-danger" name="in_confirm_delete" id="confirm-delete-button">ยืนยันการลบ</button>
                    <button data-dismiss="modal" class="btn btn-default" name="in_confirm_insert" id="cancel-delete-button">ยกเลิก</button>

                </div>

            </form>

  </article>
</section>


</body>
</html>

<?php
    }
?>