<?php
session_start();
error_reporting(0);
include '../includes/config.php';
if(strlen($_SESSION['Usernameadmin'])==0)
    {  
    header('location:../index.php');
    }else{


?>


<!DOCTYPE html>
<html lang="en">
<head>
<title>แก้ไขข้อมูลสมัครชิก</title>
<meta charset="utf-8">
<link rel="stylesheet" href="../includes/css/styles.css">
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Kanit:wght@100;200;300;400;500&family=Roboto:ital,wght@0,100;0,400;0,500;0,700;0,900;1,500;1,700;1,900&display=swap" rel="stylesheet">
<meta name="viewport" content="width=device-width, initial-scale=1">
<?php include '../includes/css/bootstrap5.php'?>
<?php

$dataw[100];
$datah[100];
$datac[100];
$databmi[100];
$dayi[100];
$dataq[10];
$Username=$_GET['User'];
$sql = "SELECT SUM(CASE WHEN t1='y' THEN 1 ELSE 0 END) as t1,SUM(CASE WHEN t2='y' THEN 1 ELSE 0 END) AS t2,SUM(CASE WHEN t3='y' THEN 1 ELSE 0 END) as t3,SUM(CASE WHEN t4='y' THEN 1 ELSE 0 END) AS t4,SUM(CASE WHEN t5='y' THEN 1 ELSE 0 END) as t5,SUM(CASE WHEN t6='y' THEN 1 ELSE 0 END) AS t6,SUM(CASE WHEN t7='y' THEN 1 ELSE 0 END) as t7,SUM(CASE WHEN t8='y' THEN 1 ELSE 0 END) AS t8,SUM(CASE WHEN t9='y' THEN 1 ELSE 0 END) AS t9 FROM polite_prescription_new WHERE  Username=:Username";
    $query = $dbh->prepare($sql);
    $query->bindParam(':Username', $Username, PDO::PARAM_STR);
    //  $query->bindParam(':Week',$Username,PDO::PARAM_STR);
    $query->execute();
    $results = $query->fetchAll(PDO::FETCH_OBJ);

    if ($query->rowCount() > 0) {
      foreach ($results as $result) {

          $dataq[1] = $result->t1;
          $dataq[2] = $result->t2;
          $dataq[3] = $result->t3;
          $dataq[4] = $result->t4;
          $dataq[5] = $result->t5;
          $dataq[6] = $result->t6;
          $dataq[7] = $result->t7;
          $dataq[8] = $result->t8;
          $dataq[9] = $result->t9;
      }
  }
  $test = array(
    array("label"=> "ข้อ 1", "y"=> $dataq[1]),
    array("label"=> "ข้อ 2", "y"=> $dataq[2]),
    array("label"=> "ข้อ 3", "y"=> $dataq[3]),
    array("label"=> "ข้อ 4", "y"=> $dataq[4]),
    array("label"=> "ข้อ 5", "y"=> $dataq[5]),
    array("label"=> "ข้อ 6", "y"=> $dataq[6]),
    array("label"=> "ข้อ 7", "y"=> $dataq[7]),
    array("label"=> "ข้อ 8", "y"=> $dataq[8]),
    array("label"=> "ข้อ 9", "y"=> $dataq[9]),
  );
  $odi = array(
    array("label"=> "ข้อ 1", "y"=> 84-$dataq[1]),
    array("label"=> "ข้อ 2", "y"=> 84-$dataq[2]),
    array("label"=> "ข้อ 3", "y"=> 84-$dataq[3]),
    array("label"=> "ข้อ 4", "y"=> 84-$dataq[4]),
    array("label"=> "ข้อ 5", "y"=> 84-$dataq[5]),
    array("label"=> "ข้อ 6", "y"=> 84-$dataq[6]),
    array("label"=> "ข้อ 7", "y"=> 84-$dataq[7]),
    array("label"=> "ข้อ 8", "y"=> 84-$dataq[8]),
    array("label"=> "ข้อ 9", "y"=> 84-$dataq[9]),
  );





    ?>
 <!DOCTYPE HTML>
 <html>
 <head>  
 <script>
      window.onload = function () {
 
 var chart = new CanvasJS.Chart("chartContainer", {
   animationEnabled: true,
   exportEnabled: true,
   theme: "light1", // "light1", "light2", "dark1", "dark2"
   title:{
     text: "รายงานสรุปผลทั้งหมดของการบันทึก"
   },
   axisX:{
     reversed: true
   },
   axisY:{
     includeZero: true
   },
   toolTip:{
     shared: true
   },
   data: [{
     type: "stackedBar",
     name: "ทำ",
     dataPoints: <?php echo json_encode($test, JSON_NUMERIC_CHECK); ?>
   },{
     type: "stackedBar",
     name: "ไม่ทำ",
     dataPoints: <?php echo json_encode($odi, JSON_NUMERIC_CHECK); ?>
   }]
 });
 chart.render();
  
 }
      </script>
</head>
<body>
<?php include '../includes/admin/header.php'?>
<section>
<?php include '../includes/admin/sidebar.php'?>


<article>

<div class="container">
<br>
     <br>
    <div id="chartContainer" style="height: 500px; width: 100%; "  class="mb-5"></div>
<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>

    </div>


    
</div>
  </article>
</section>


</body>
</html>

<?php
    }
?>