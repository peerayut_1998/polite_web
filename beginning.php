<?php
session_start();
error_reporting(0);
include 'includes/config.php';
if (strlen($_SESSION['Username']) == 0) {
    header('location:index.php');
} else {
    if (isset($_POST['go'])) {
        header('location:performancereport.php');
    }
    if (isset($_POST['no'])) {
        header('location:logout.php');
    }

?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Kanit:wght@100;200;300;400;500&family=Roboto:ital,wght@0,100;0,400;0,500;0,700;0,900;1,500;1,700;1,900&display=swap" rel="stylesheet">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>ABC Health Fitness</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <script src="includes/js/bootstrap.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.bundle.min.js"></script>
        <script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
    </head>
    <?php
    $Username = $_SESSION['Username'];
    ?>
    <!DOCTYPE HTML>
    <html>

    <style>
        * {}
    </style>
    <?php include 'includes/headerwork.php'; ?>

    <body>
        <main>
            <div class="p-3 mb-2 bg-secondary text-white">
                <div class="d-flex justify-content-between">
                    <div class="item" style="margin-right:  50px; font-size: 20px;">
                        <a href="#" class="nav-link  text-white">หน้าแสดงผลสมาชิก</a>
                    </div>
                    <div class="item" style="margin-right:  50px; font-size: 20px;">
                        <a href="index.php" class="nav-link  text-white">หน้าหลัก</a>
                    </div>
                </div>
            </div>
            <div class="container">
                <br>
                <br>
                <form method="post">
                    <div class="form-row">
                        <div class="col-md-4">
                            <label for="weight">วัน/เดือน/ปี</label>
                            <input type="text" class="form-control" id="day" name="day" value="<?php echo date("d/m/Y") ?>" disabled>
                        </div>
                    </div>
                    <br>
                    <div class="form-row">
                        <div class="col-md-4">
                            <label for="weight">น้ำหนัก</label>
                            <input type="number" class="form-control" id="weight" name="weight" placeholder="weight" value="0" oninput="calculate()">
                        </div>
                        <div class="col-md-4 offset-md-4">
                            <label for="height">ส่วนสูง</label>
                            <input type="number" class="form-control" id="height" name="height" placeholder="height" value="0" oninput="calculate()">
                        </div>
                    </div>
                    <br>
                    <div class="form-row">
                        <div class="col-md-4">
                            <label for="bmi">BMI</label>
                            <input type="text" class="form-control" id="bmi" name="bmi" placeholder="bmi" value="0" disabled>

                        </div>
                        <div class="col-md-4 offset-md-4">
                            <label for="waistline">รอบเอว</label>
                            <input type="number" class="form-control" id="waistline" placeholder="waistline">
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="d-flex justify-content-center">

                                <a class="image-popup-fit-width" href="admin/img/main/1011.png" >
                                    <img src="admin/img/main/1011.png" class="img-thumbnail" height="400" width="400" alt="...">
                                </a>
                            </div>

                        </div>
                        <div class="col-md-4 offset-md-4">
                            <div class="d-flex justify-content-center">
                            <a class="image-popup-fit-width" href="admin/img/main/n1.png" >
                            <img src="admin/img/main/n1.png" class="img-thumbnail" height="400" width="400" alt="...">
                                </a>
                                
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row ">
                        <div class="col-sm">
                            <div class="d-flex justify-content-center">
                                <img src="admin/img/main/442.png" class="img-thumbnail" alt="...">
                            </div>
                        </div>
                        <div class="col-sm ">
                            <div class="d-flex justify-content-center align-content-center text-center">
                                <h5>
                                    พร้อม หุ่นดี สุขภาพดี รึยังค่ะ
                                    ถ้าพร้อมแล้ว GO
                                    </h3>
                            </div>
                        </div>
                        <div class="col-sm">
                            <div class="d-flex justify-content-center">
                                <img src="admin/img/main/51.png" class="img-thumbnail" alt="...">
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="d-flex ">
                            <div class="mr-auto p-5  mx-5">
                                <button class="btn btn-primary" type="submit" name="no" id="no">NO</button>

                            </div>
                            <div class="p-5 mx-5">
                                <button class="btn btn-primary" type="submit" name="go" id="go">GO</button>
                            </div>
                        </div>
                    </div>
                </form>


            </div>
        </main>
        <script>
            function calculate() {
                var bmi;
                var result = document.getElementById("bmi");
                var weight = parseInt(document.getElementById("weight").value);
                var height = parseInt(document.getElementById("height").value);
                bmi = (weight / Math.pow((height / 100), 2)).toFixed(1);

                if (height === "" || isNaN(height))
                    result.value = "Provide a valid Height!";

                else if (weight === "" || isNaN(weight))
                    result.value = "Provide a valid Weight!";

                // If both input is valid, calculate the bmi
                else {

                    // Fixing upto 2 decimal places
                    let bmi = (weight / ((height * height) / 10000)).toFixed(2);
                    result.value = bmi;
                    // Dividing as per the bmi conditions
                }
            };
        </script>
    </body>
    <?php include 'includes/footer.php' ?>

    </html>

<?php
}
?>