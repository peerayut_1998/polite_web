<?php
session_start();
error_reporting(0);
include 'includes/config.php';
if (isset($_POST['submit'])) {
  $Username = (string)$_POST['Username'];
  $sql = "SELECT * from polite_user where Username=:Username";
  $query = $dbh->prepare($sql);
  $query->bindParam(':Username', $Username, PDO::PARAM_STR);
  $query->execute();
  $results = $query->fetchAll(PDO::FETCH_OBJ);

  if ($query->rowCount() > 0) {
    echo "<script type='text/javascript'>alert('Username มีผู้ใช้แล้ว');</script>";
  } else {
    $First_Name = $_POST['First_Name'];
    $Last_Name = $_POST['Last_Name'];
    $age = (string)$_POST['age'];
    $gender = (string)$_POST['gender'];
    $Phone_Number = $_POST['Phone_Number'];
    $Password = md5($_POST['Password']);
    $dt = date('Y-m-d');
    function generateRandomString($length = 5)
    {
      $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
      $charactersLength = strlen($characters);
      $randomString = '';
      for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
      }
      return $randomString;
    }


    $sql = "INSERT INTO polite_user(Username, Password,First_Name,Last_Name, age,gender,Phone_Number,application_date) VALUES (:Username, :Password, :First_Name,:Last_Name,:age,:gender, :Phone_Number,:application_date);";
    $query = $dbh->prepare($sql);
    $query->bindParam(':First_Name', $First_Name, PDO::PARAM_STR);
    $query->bindParam(':Last_Name', $Last_Name, PDO::PARAM_STR);
    $query->bindParam(':age', $age, PDO::PARAM_STR);
    $query->bindParam(':gender', $gender, PDO::PARAM_STR);
    $query->bindParam(':Phone_Number', $Phone_Number, PDO::PARAM_STR);
    $query->bindParam(':Username', $Username, PDO::PARAM_STR);
    $query->bindParam(':Password', $Password, PDO::PARAM_STR);
    $query->bindParam(':application_date', $dt, PDO::PARAM_STR);
    $query->execute();
    $lastInsertId = $dbh->lastInsertId();


    if ($lastInsertId) {
      $msg = "User applied successfully";
    } else {
      $error = "Something went wrong. Please try again";
    }
    $sql = "INSERT INTO polite_prescription_new ( Week, days, t1, t2, t3, t4,t5, t6, t7, t8, t9, data, Username) VALUES 
    ('1','1','q','q','q','q','q','q','q','q','q',:application_date,:Username),
    ('1','2','q','q','q','q','q','q','q','q','q',:application_date,:Username),
    ('1','3','q','q','q','q','q','q','q','q','q',:application_date,:Username),
    ('1','4','q','q','q','q','q','q','q','q','q',:application_date,:Username),
    ('1','5','q','q','q','q','q','q','q','q','q',:application_date,:Username),
    ('1','6','q','q','q','q','q','q','q','q','q',:application_date,:Username),
    ('1','7','q','q','q','q','q','q','q','q','q',:application_date,:Username),
    ('2','8','q','q','q','q','q','q','q','q','q',:application_date,:Username),
    ('2','9','q','q','q','q','q','q','q','q','q',:application_date,:Username),
    ('2','10','q','q','q','q','q','q','q','q','q',:application_date,:Username),
    ('2','11','q','q','q','q','q','q','q','q','q',:application_date,:Username),
    ('2','12','q','q','q','q','q','q','q','q','q',:application_date,:Username),
    ('2','13','q','q','q','q','q','q','q','q','q',:application_date,:Username),
    ('2','14','q','q','q','q','q','q','q','q','q',:application_date,:Username),
    ('3','15','q','q','q','q','q','q','q','q','q',:application_date,:Username),
    ('3','16','q','q','q','q','q','q','q','q','q',:application_date,:Username),
    ('3','17','q','q','q','q','q','q','q','q','q',:application_date,:Username),
    ('3','18','q','q','q','q','q','q','q','q','q',:application_date,:Username),
    ('3','19','q','q','q','q','q','q','q','q','q',:application_date,:Username),
    ('3','20','q','q','q','q','q','q','q','q','q',:application_date,:Username),
    ('3','21','q','q','q','q','q','q','q','q','q',:application_date,:Username),
    ('4','22','q','q','q','q','q','q','q','q','q',:application_date,:Username),
    ('4','23','q','q','q','q','q','q','q','q','q',:application_date,:Username),
    ('4','24','q','q','q','q','q','q','q','q','q',:application_date,:Username),
    ('4','25','q','q','q','q','q','q','q','q','q',:application_date,:Username),
    ('4','26','q','q','q','q','q','q','q','q','q',:application_date,:Username),
    ('4','27','q','q','q','q','q','q','q','q','q',:application_date,:Username),
    ('4','28','q','q','q','q','q','q','q','q','q',:application_date,:Username),
    ('5','29','q','q','q','q','q','q','q','q','q',:application_date,:Username),
    ('5','30','q','q','q','q','q','q','q','q','q',:application_date,:Username),
    ('5','31','q','q','q','q','q','q','q','q','q',:application_date,:Username),
    ('5','32','q','q','q','q','q','q','q','q','q',:application_date,:Username),
    ('5','33','q','q','q','q','q','q','q','q','q',:application_date,:Username),
    ('5','34','q','q','q','q','q','q','q','q','q',:application_date,:Username),
    ('5','35','q','q','q','q','q','q','q','q','q',:application_date,:Username),
    ('6','36','q','q','q','q','q','q','q','q','q',:application_date,:Username),
    ('6','37','q','q','q','q','q','q','q','q','q',:application_date,:Username),
    ('6','38','q','q','q','q','q','q','q','q','q',:application_date,:Username),
    ('6','39','q','q','q','q','q','q','q','q','q',:application_date,:Username),
    ('6','40','q','q','q','q','q','q','q','q','q',:application_date,:Username),
    ('6','41','q','q','q','q','q','q','q','q','q',:application_date,:Username),
    ('6','42','q','q','q','q','q','q','q','q','q',:application_date,:Username),
    ('7','43','q','q','q','q','q','q','q','q','q',:application_date,:Username),
    ('7','44','q','q','q','q','q','q','q','q','q',:application_date,:Username),
    ('7','45','q','q','q','q','q','q','q','q','q',:application_date,:Username),
    ('7','46','q','q','q','q','q','q','q','q','q',:application_date,:Username),
    ('7','47','q','q','q','q','q','q','q','q','q',:application_date,:Username),
    ('7','48','q','q','q','q','q','q','q','q','q',:application_date,:Username),
    ('7','49','q','q','q','q','q','q','q','q','q',:application_date,:Username),
    ('8','50','q','q','q','q','q','q','q','q','q',:application_date,:Username),
    ('8','51','q','q','q','q','q','q','q','q','q',:application_date,:Username),
    ('8','52','q','q','q','q','q','q','q','q','q',:application_date,:Username),
    ('8','53','q','q','q','q','q','q','q','q','q',:application_date,:Username),
    ('8','54','q','q','q','q','q','q','q','q','q',:application_date,:Username),
    ('8','55','q','q','q','q','q','q','q','q','q',:application_date,:Username),
    ('8','56','q','q','q','q','q','q','q','q','q',:application_date,:Username),
    ('9','57','q','q','q','q','q','q','q','q','q',:application_date,:Username),
    ('9','58','q','q','q','q','q','q','q','q','q',:application_date,:Username),
    ('9','59','q','q','q','q','q','q','q','q','q',:application_date,:Username),
    ('9','60','q','q','q','q','q','q','q','q','q',:application_date,:Username),
    ('9','61','q','q','q','q','q','q','q','q','q',:application_date,:Username),
    ('9','62','q','q','q','q','q','q','q','q','q',:application_date,:Username),
    ('9','63','q','q','q','q','q','q','q','q','q',:application_date,:Username),
    ('10','64','q','q','q','q','q','q','q','q','q',:application_date,:Username),
    ('10','65','q','q','q','q','q','q','q','q','q',:application_date,:Username),
    ('10','66','q','q','q','q','q','q','q','q','q',:application_date,:Username),
    ('10','67','q','q','q','q','q','q','q','q','q',:application_date,:Username),
    ('10','68','q','q','q','q','q','q','q','q','q',:application_date,:Username),
    ('10','69','q','q','q','q','q','q','q','q','q',:application_date,:Username),
    ('10','70','q','q','q','q','q','q','q','q','q',:application_date,:Username),
    ('11','71','q','q','q','q','q','q','q','q','q',:application_date,:Username),
    ('11','72','q','q','q','q','q','q','q','q','q',:application_date,:Username),
    ('11','73','q','q','q','q','q','q','q','q','q',:application_date,:Username),
    ('11','74','q','q','q','q','q','q','q','q','q',:application_date,:Username),
    ('11','75','q','q','q','q','q','q','q','q','q',:application_date,:Username),
    ('11','76','q','q','q','q','q','q','q','q','q',:application_date,:Username),
    ('11','77','q','q','q','q','q','q','q','q','q',:application_date,:Username),
    ('12','78','q','q','q','q','q','q','q','q','q',:application_date,:Username),
    ('12','79','q','q','q','q','q','q','q','q','q',:application_date,:Username),
    ('12','80','q','q','q','q','q','q','q','q','q',:application_date,:Username),
    ('12','81','q','q','q','q','q','q','q','q','q',:application_date,:Username),
    ('12','82','q','q','q','q','q','q','q','q','q',:application_date,:Username),
    ('12','83','q','q','q','q','q','q','q','q','q',:application_date,:Username),
    ('12','84','q','q','q','q','q','q','q','q','q',:application_date,:Username);
    INSERT INTO polite_obstacle( Week, days, Username) VALUES 
('1','1',:Username),
('1','2',:Username),
('1','3',:Username),
('1','4',:Username),
('1','5',:Username),
('1','6',:Username),
('1','7',:Username),
('2','8',:Username),
('2','9',:Username),
('2','10',:Username),
('2','11',:Username),
('2','12',:Username),
('2','13',:Username),
('2','14',:Username),
('3','15',:Username),
('3','16',:Username),
('3','17',:Username),
('3','18',:Username),
('3','19',:Username),
('3','20',:Username),
('3','21',:Username),
('4','22',:Username),
('4','23',:Username),
('4','24',:Username),
('4','25',:Username),
('4','26',:Username),
('4','27',:Username),
('4','28',:Username),
('5','29',:Username),
('5','30',:Username),
('5','31',:Username),
('5','32',:Username),
('5','33',:Username),
('5','34',:Username),
('5','35',:Username),
('6','36',:Username),
('6','37',:Username),
('6','38',:Username),
('6','39',:Username),
('6','40',:Username),
('6','41',:Username),
('6','42',:Username),
('7','43',:Username),
('7','44',:Username),
('7','45',:Username),
('7','46',:Username),
('7','47',:Username),
('7','48',:Username),
('7','49',:Username),
('8','50',:Username),
('8','51',:Username),
('8','52',:Username),
('8','53',:Username),
('8','54',:Username),
('8','55',:Username),
('8','56',:Username),
('9','57',:Username),
('9','58',:Username),
('9','59',:Username),
('9','60',:Username),
('9','61',:Username),
('9','62',:Username),
('9','63',:Username),
('10','64',:Username),
('10','65',:Username),
('10','66',:Username),
('10','67',:Username),
('10','68',:Username),
('10','69',:Username),
('10','70',:Username),
('11','71',:Username),
('11','72',:Username),
('11','73',:Username),
('11','74',:Username),
('11','75',:Username),
('11','76',:Username),
('11','77',:Username),
('12','78',:Username),
('12','79',:Username),
('12','80',:Username),
('12','81',:Username),
('12','82',:Username),
('12','83',:Username),
('12','84',:Username);";
    $dt = date('Y-m-d');
    $query = $dbh->prepare($sql);
    $query->bindParam(':Username', $Username, PDO::PARAM_STR);
    $query->bindParam(':application_date', $dt, PDO::PARAM_STR);

    $query->execute();
    $lastInsertId = $dbh->lastInsertId();


    if ($lastInsertId) {
      echo "<script type='text/javascript'>alert('สมัครสมาชิกเสร็จสิ้น');</script>";
      header('location:login.php');
    } else {
      $error = "error 400";
    }
  }
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Kanit:wght@100;200;300;400;500&family=Roboto:ital,wght@0,100;0,400;0,500;0,700;0,900;1,500;1,700;1,900&display=swap" rel="stylesheet">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>สมัครสมาชิก</title>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <script src="includes/js/bootstrap.js"></script>
</head>
<style>
  * {}
</style>
<?php include 'includes/header.php'; ?>

<body>
  <main>
    <div class="p-3 mb-2 bg-secondary text-white">
      <div class="d-flex justify-content-between">
        <div class="item" style="margin-right:  50px; font-size: 20px;">
          <a href="#" class="nav-link  text-white">สมัครสมาชิก</a>
        </div>
        <div class="item" style="margin-right:  50px; font-size: 20px;">
          <a href="index.php" class="nav-link  text-white">หน้าหลัก</a>
        </div>
      </div>
    </div>
    <div class="container">
      <div>
        <br>
        <form method="post">

          <div class="form-group">
            <label for="inputName">ชื่อ</label>
            <input type="text" class="form-control" id="First_Name" name="First_Name" placeholder="กรอกชื่อ" required>

          </div>

          <div class="form-group">
            <label for="inputName">นามสกุล</label>
            <input type="text" class="form-control" id="Last_Name" name="Last_Name" placeholder="กรอกนามสกุล" required>

          </div>

          <div class="form-group">
            <label for="inputPhone_number">อายุ</label>
            <input type="number" class="form-control" id="age" name="age" placeholder="กรอก อายุ" onKeyPress="if(this.value.length==2) return false;" required>

          </div>

          <div class="form-group">
            <label for="inputName">เพศ</label>
            <select class="form-select" aria-label="Default select example" name="gender" required>
              <option value="ชาย">ชาย</option>
              <option value="หญิง">หญิง</option>

            </select>
          </div>

      </div>
      <div class="form-group">
        <label for="inputPhone_number">เบอร์โทร</label>
        <input type="text" class="form-control" id="Phone_Number" name="Phone_Number" placeholder="กรอกเบอร์โทร" required>

      </div>
      <div class="form-group">
        <label for="inputPhone_number">Username</label>
        <input type="number" class="form-control" id="Username" name="Username" placeholder="กรอก Username" minlength="5" onKeyPress="if(this.value.length==5) return false;" required>

      </div>
      <div class="form-group">
        <label for="inputPassword">Password</label>
        <input type="Password" class="form-control" id="Password" minlength="8" name="Password" placeholder="กรอก Password" required>

      </div>
      <div class="col text-center">
        <button type="submit" class="btn btn-primary " name="submit" id="submit" onclick="return confirm('ยืนยันการสมัครสมาชิก')">บันทึกการสมัครสมาชิก</button>
      </div>
      <br>
    </div>
    </div>
  </main>
</body>
<footer>
  <?php include 'includes/footer.php' ?>
</footer>

</html>