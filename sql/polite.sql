-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 17, 2022 at 10:25 AM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 8.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `polite`
--

-- --------------------------------------------------------

--
-- Table structure for table `polite_admin`
--

CREATE TABLE `polite_admin` (
  `Id` int(11) NOT NULL,
  `Username` varchar(25) NOT NULL,
  `Password` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `polite_admin`
--

INSERT INTO `polite_admin` (`Id`, `Username`, `Password`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3');

-- --------------------------------------------------------

--
-- Table structure for table `polite_knowledge`
--

CREATE TABLE `polite_knowledge` (
  `Id` int(11) NOT NULL,
  `Section` varchar(200) NOT NULL,
  `News_details` varchar(1000) NOT NULL,
  `Image_file` varchar(60) NOT NULL,
  `Datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `polite_knowledge`
--

INSERT INTO `polite_knowledge` (`Id`, `Section`, `News_details`, `Image_file`, `Datetime`) VALUES
(4, 'ad', 'asdaasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasasas', 'g8bblwk2j1r0dfhdsr9qlzuopbu6dr91ooy2vllg4uwrxlrewt.jfif', '2021-10-15 17:52:05'),
(5, 'wqe', 'we', '84p8xjusccz9z4da7p28j1xz61y8effmiz32vx4kwmv3mtljig.png', '2021-12-22 09:32:11');

-- --------------------------------------------------------

--
-- Table structure for table `polite_obstacle`
--

CREATE TABLE `polite_obstacle` (
  `Id` int(11) NOT NULL,
  `details` varchar(200) NOT NULL,
  `Week` int(2) NOT NULL,
  `Username` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `polite_obstacle`
--

INSERT INTO `polite_obstacle` (`Id`, `details`, `Week`, `Username`) VALUES
(194, ' ', 1, 'admin'),
(195, ' ', 2, 'admin'),
(196, ' ', 3, 'admin'),
(197, ' ', 4, 'admin'),
(198, ' ', 5, 'admin'),
(199, ' ', 6, 'admin'),
(200, ' ', 7, 'admin'),
(201, ' ', 8, 'admin'),
(202, ' ', 9, 'admin'),
(203, ' ', 10, 'admin'),
(204, ' ', 11, 'admin'),
(205, ' ', 12, 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `polite_prescription`
--

CREATE TABLE `polite_prescription` (
  `Id` int(11) NOT NULL,
  `Week` int(2) NOT NULL,
  `List` varchar(50) NOT NULL,
  `Monday` int(1) NOT NULL,
  `Tuesday` int(1) NOT NULL,
  `Wednesday` int(1) NOT NULL,
  `Thursday` int(1) NOT NULL,
  `Friday` int(1) NOT NULL,
  `Saturday` int(1) NOT NULL,
  `Sunday` int(1) NOT NULL,
  `Username` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `polite_prescription`
--

INSERT INTO `polite_prescription` (`Id`, `Week`, `List`, `Monday`, `Tuesday`, `Wednesday`, `Thursday`, `Friday`, `Saturday`, `Sunday`, `Username`) VALUES
(1945, 1, '011', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(1946, 1, '012', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(1947, 1, '013', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(1948, 1, '014', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(1949, 1, '015', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(1950, 1, '016', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(1951, 1, '021', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(1952, 1, '022', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(1953, 1, '023', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(1954, 2, '011', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(1955, 2, '012', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(1956, 2, '013', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(1957, 2, '014', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(1958, 2, '015', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(1959, 2, '016', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(1960, 2, '021', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(1961, 2, '022', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(1962, 2, '023', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(1963, 3, '011', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(1964, 3, '012', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(1965, 3, '013', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(1966, 3, '014', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(1967, 3, '015', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(1968, 3, '016', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(1969, 3, '021', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(1970, 3, '022', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(1971, 3, '023', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(1972, 4, '011', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(1973, 4, '012', 1, 0, 0, 0, 0, 0, 0, 'admin'),
(1974, 4, '013', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(1975, 4, '014', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(1976, 4, '015', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(1977, 4, '016', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(1978, 4, '021', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(1979, 4, '022', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(1980, 4, '023', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(1981, 5, '011', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(1982, 5, '012', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(1983, 5, '013', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(1984, 5, '014', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(1985, 5, '015', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(1986, 5, '016', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(1987, 5, '021', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(1988, 5, '022', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(1989, 5, '023', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(1990, 6, '011', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(1991, 6, '012', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(1992, 6, '013', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(1993, 6, '014', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(1994, 6, '015', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(1995, 6, '016', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(1996, 6, '021', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(1997, 6, '022', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(1998, 6, '023', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(1999, 7, '011', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(2000, 7, '012', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(2001, 7, '013', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(2002, 7, '014', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(2003, 7, '015', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(2004, 7, '016', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(2005, 7, '021', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(2006, 7, '022', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(2007, 7, '023', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(2008, 8, '011', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(2009, 8, '012', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(2010, 8, '013', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(2011, 8, '014', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(2012, 8, '015', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(2013, 8, '016', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(2014, 8, '021', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(2015, 8, '022', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(2016, 8, '023', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(2017, 9, '011', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(2018, 9, '012', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(2019, 9, '013', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(2020, 9, '014', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(2021, 9, '015', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(2022, 9, '016', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(2023, 9, '021', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(2024, 9, '022', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(2025, 9, '023', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(2026, 10, '011', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(2027, 10, '012', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(2028, 10, '013', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(2029, 10, '014', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(2030, 10, '015', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(2031, 10, '016', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(2032, 10, '021', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(2033, 10, '022', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(2034, 10, '023', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(2035, 11, '011', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(2036, 11, '012', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(2037, 11, '013', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(2038, 11, '014', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(2039, 11, '015', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(2040, 11, '016', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(2041, 11, '021', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(2042, 11, '022', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(2043, 11, '023', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(2044, 12, '011', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(2045, 12, '012', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(2046, 12, '013', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(2047, 12, '014', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(2048, 12, '015', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(2049, 12, '016', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(2050, 12, '021', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(2051, 12, '022', 0, 0, 0, 0, 0, 0, 0, 'admin'),
(2052, 12, '023', 0, 0, 0, 0, 0, 0, 0, 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `polite_pressrelease`
--

CREATE TABLE `polite_pressrelease` (
  `Id` int(11) NOT NULL,
  `Section` varchar(200) NOT NULL,
  `News_details` varchar(1000) NOT NULL,
  `Image_file` varchar(60) NOT NULL,
  `Datetime` datetime NOT NULL,
  `Link` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `polite_pressrelease`
--

INSERT INTO `polite_pressrelease` (`Id`, `Section`, `News_details`, `Image_file`, `Datetime`, `Link`) VALUES
(86, 'sd', 'asad', 'ca2qrhurb6hujztmg9y73kkzc53kyryilhcdkkcgxi17bpww3l.jfif', '2021-10-15 17:50:21', 'sdasda'),
(87, 'sssssssssssssssssssssssssssssssssss', 'sssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss', 'mc40ax6d3rsl2jw20dce8jgr91nxcj9aedevu8qd49g20f3u1p.png', '2021-10-15 17:50:37', 'asdad');

-- --------------------------------------------------------

--
-- Table structure for table `polite_recommend`
--

CREATE TABLE `polite_recommend` (
  `Id` int(11) NOT NULL,
  `Name` varchar(255) NOT NULL,
  `Image_file` varchar(255) NOT NULL,
  `Datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `polite_user`
--

CREATE TABLE `polite_user` (
  `Id` int(11) NOT NULL,
  `Code_User` varchar(10) NOT NULL,
  `Username` varchar(20) NOT NULL,
  `Password` varchar(100) NOT NULL,
  `First_Name` varchar(100) NOT NULL,
  `Last_Name` varchar(100) NOT NULL,
  `Phone_Number` varchar(10) NOT NULL,
  `application_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `polite_user`
--

INSERT INTO `polite_user` (`Id`, `Code_User`, `Username`, `Password`, `First_Name`, `Last_Name`, `Phone_Number`, `application_date`) VALUES
(44, 'V3NXK', 'admin', '25d55ad283aa400af464c76d713c07ad', 'peerayut', 'mongpaek', '+669269532', '2022-02-17');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `polite_admin`
--
ALTER TABLE `polite_admin`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `polite_knowledge`
--
ALTER TABLE `polite_knowledge`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `polite_obstacle`
--
ALTER TABLE `polite_obstacle`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `polite_prescription`
--
ALTER TABLE `polite_prescription`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `polite_pressrelease`
--
ALTER TABLE `polite_pressrelease`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `polite_recommend`
--
ALTER TABLE `polite_recommend`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `polite_user`
--
ALTER TABLE `polite_user`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `Code_User` (`Code_User`),
  ADD UNIQUE KEY `Username` (`Username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `polite_admin`
--
ALTER TABLE `polite_admin`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `polite_knowledge`
--
ALTER TABLE `polite_knowledge`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `polite_obstacle`
--
ALTER TABLE `polite_obstacle`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=206;

--
-- AUTO_INCREMENT for table `polite_prescription`
--
ALTER TABLE `polite_prescription`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2053;

--
-- AUTO_INCREMENT for table `polite_pressrelease`
--
ALTER TABLE `polite_pressrelease`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;

--
-- AUTO_INCREMENT for table `polite_recommend`
--
ALTER TABLE `polite_recommend`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `polite_user`
--
ALTER TABLE `polite_user`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
