




<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Kanit:wght@100;200;300;400;500&family=Roboto:ital,wght@0,100;0,400;0,500;0,700;0,900;1,500;1,700;1,900&display=swap" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>ใบสั่งสุขภาพคืออะไร</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <script src="includes/js/bootstrap.js"></script>  
</head>
<style>
  *{

  }

</style>
<?php include 'includes/header.php'; ?>

<body>
  <main>
<div class="p-3 mb-2 bg-secondary text-white"><div class="d-flex justify-content-between">
<div class="item" style="margin-right:  50px; font-size: 20px;">
                    <a href="#" class="nav-link  text-white">ใบสั่งสุขภาพคืออะไร</a>
                   
                </div>
<div class="item" style="margin-right:  50px; font-size: 20px;">
                    <a href="index.php" class="nav-link  text-white">หน้าหลัก</a>
                    
                </div>
 </div> 
</div>
<div class="container">
  <br>
  <br>
  <h3 class="text-center">ใบสั่งสุขภาพร่างกายคืออะไร</h3>
  <div>
  <br class="p-2">
  <p class="text-center">          ใบสั่งสุขภาพร่างกายคือ การปรับเปลี่ยนพฤติกรรมสุขภาพ จัดทำโดยคลินิกปรับเลี่ยน
     พฤติกรรมสุขภาพ โรงพยาบาลส่งเสริมสุขภาพแม่และเด็กศูนย์อนามัยที่ 3 นครสวรรค์<br>
     มีวัตถุประสงค์ เพื่อเผยแพร่ใบสังสุขภาพ และเป็นเว็บไซต์การเปลี่ยนแปลงของน้ำหนัก
     รอบเอว BMI และพฤติกรรมตนเองทั้ง 5 ด้านให้แก่เจ้าหน้าที่สาธารณสุขและผู้เกี่ยวข้อง
     <br>นำไปใช้เป็นแนวทางในการดำเนินงานปรับเปลี่ยนพฤติกรรมสุขภาพให้ประชาชนทั่วไป ทุก
     กลุ่มทุกอาชีพ ทั้งผู้ที่มีสุขภาพดีอยู่แล้วหรือผู้ที่เจ็บป่วยเล็กน้อยก็ตาม <br>ได้เคลื่อนไหวออก
     แรง ออกกำลังกาย และเลือกรับประทานอาหารอย่างเหมาะสมกับตนเอง เพื่อสุขภาพที่
     สมบูรณ์แข็งแรง
     <br>
     <br>
           Body Mass Index (BMI) มีสูตรการคำนวณ = น้ำหนักตัว[Kg] / (ส่วนสูง[m] ยกกำลังสอง) 
     สูตรคำนวณเหมาะสำหรับใช้ประเมินผู้ที่มีอายุตั้งแต่ 20 ปีขึ้นไป ประโยชน์ของการวัดค่า BMI 
     เพื่อดูอัตราเสี่ยงต่อการเกิดโรคต่างๆ ตรวจสอบภาวะไขมันและความอ้วน   ดังนั้นการทำให้ร่างกายอยู่ใน
เกณฑ์ปกติ จึงมีความสำคัญอย่างยิ่งกับผู้ที่ต้องการรักษาสุขภาพในระยะยาว
<br>
ต้ำหนัก ต่ำกว่าเกณ = <18.5
<br>
น้ำหนัก ปกติ = 18.5–24.9
<br>
น้ำหนัก อ้วน = 25–29.9
<br>
น้ำหนัก เกินพิกัด = 30 หรือมากกว่า 
<br>
</p>
  <br>
  <br>
  </div>

<div>
  <br>
  </main>
</body>
<footer>
<?php include 'includes/footer.php'?>
</footer>

</html>