<?php
include 'includes/config.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Kanit:wght@100;200;300;400;500&family=Roboto:ital,wght@0,100;0,400;0,500;0,700;0,900;1,500;1,700;1,900&display=swap" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>องค์ความรู้</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <script src="includes/js/bootstrap.js"></script>  
</head>
<style>
  *{

  }

</style>
<?php include 'includes/header.php'; ?>
<div class="p-3 mb-2 bg-secondary text-white"><div class="d-flex justify-content-between">
<div class="item" style="margin-right:  50px; font-size: 20px;">
                    <a href="#" class="nav-link  text-white">องค์ความรู้</a>
                </div>
<div class="item" style="margin-right:  50px; font-size: 20px;">
                    <a href="index.php" class="nav-link  text-white">หน้าหลัก</a>
                </div>
 </div> 
</div>

<body>
<div class="container">
<div class="flex-column d-flex justify-content-between">

<?php $sql = "SELECT * from polite_knowledge";
$query = $dbh -> prepare($sql);
$query->execute();
$results=$query->fetchAll(PDO::FETCH_OBJ);
$cnt=1;
if($query->rowCount() > 0)
{
  function DateThai($strDate)
	{
		$strYear = date("Y",strtotime($strDate))+543;
		$strMonth= date("n",strtotime($strDate));
		$strDay= date("j",strtotime($strDate));
		$strHour= date("H",strtotime($strDate));
		$strMinute= date("i",strtotime($strDate));
		$strSeconds= date("s",strtotime($strDate));
		$strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
		$strMonthThai=$strMonthCut[$strMonth];
		return "$strDay $strMonthThai $strYear, $strHour:$strMinute น.";
	}
    foreach($results as $result)
    { 
       

?>
<div class="card m-3" style="width: 500; ">

<img src="admin/img/<?php echo htmlentities($result->Image_file)?>"  style="display: block; margin: 0 auto; "class="center" width="400" >
  <div class="card-body">
    <h5 class="card-title"><?php echo htmlentities($result->Section)?></h5>
    <div class="mb-1 text-muted"><?php echo DateThai($result->Datetime)?></div>
    <div class="d-flex justify-content-end"> <a href="detailknowledge.php?Id=<?php echo htmlentities($result->Id);?>" class="btn btn-warning">อ่านเพิ่มเติม</a></div>
   
  </div>
</div>
<?php
    }

}

  ?>

</div>
</div>
   
</body>
<?php include 'includes/footer.php'?>
</html>