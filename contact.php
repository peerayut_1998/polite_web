<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Kanit:wght@100;200;300;400;500&family=Roboto:ital,wght@0,100;0,400;0,500;0,700;0,900;1,500;1,700;1,900&display=swap" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>ติดต่อเรา</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <script src="includes/js/bootstrap.js"></script>  
</head>
<?php include 'includes/header.php'; ?>

<body>
  <main>
    <div class="container">
       
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15365.439997119593!2d100.12106560949493!3d15.67903297807221!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30e04f9831892e31%3A0x69cc47d6e130fd71!2sABC%20Health%20Fitness!5e0!3m2!1sth!2sth!4v1634193491987!5m2!1sth!2sth" width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
        <div class="card" style="width: 100%; ">


  <div class="card-body">
  <div class="container">
  <div class="row">
  <div class="col-1">
  <img class="img-responsive" src="admin/img/main/facebook.png" width="75px" height="75px" >
    </div>
    <div class="col-2">
      <p style="font-size:25px">Facebook</p>
      <a href="1.........................."><p style="color:Orange; font-size:14px ">ABC Health Fitness </p></a>
    </div>
    <div class="col-1">
  <img class="img-responsive" src="admin/img/main/email.png" width="75px" height="75px" >
    </div>
    <div class="col-2">
      <p style="font-size:25px">Email:</p>
      <p style="color:Orange; font-size:14px ">abcfitness_hpc3@gmail.com</p>
    </div>
    <div class="col-1">
  <img class="img-responsive" src="admin/img/main/call.png" width="75px" height="75px" >
    </div>
    <div class="col-2">
      <p style="font-size:25px">Call:</p>
      <p style="color:Orange; font-size:14px ">056 255 451 ต่อ 154</p>
    </div>
    <div class="col-1">
  <img class="img-responsive" src="admin/img/main/youtube.png" width="75px" height="75px" >
    </div>
    <div class="col-2">
      <p style="font-size:25px">Youtube</p>
      <a href="2........................"><p style="color:Orange; font-size:14px ">ABC  Fitness <br>ศูนย์อนามัยที่ 3 <br>นครสวรนค์</p></a> 
    </div>
  </div>
</div>
    
  </div>
  </div>
    </main>
</body>

<footer>
<?php include 'includes/footer.php'?>
</footer>

</html>

