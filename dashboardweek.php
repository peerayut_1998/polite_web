<?php
session_start();
error_reporting(0);
include 'includes/config.php';
if (strlen($_SESSION['Username']) == 0) {
  header('location:index.php');
} else {
  $Gday = $_SESSION['Gday'];
  $week = $_GET['week'];

  $d1 = (($week - 1) * 7) + 1;
  $d2 = ($week * 7);
  if (isset($_POST['submitday'])) {
    $daydd = $_POST['daydd'];
  }
?>

  <!DOCTYPE html>
  <html lang="en">

  <head>
    <meta charset="UTF-8">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Kanit:wght@100;200;300;400;500&family=Roboto:ital,wght@0,100;0,400;0,500;0,700;0,900;1,500;1,700;1,900&display=swap" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>ABC Health Fitness</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <script src="includes/js/bootstrap.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.bundle.min.js"></script>
    <?php
    $dataw[10];
    $datah[10];
    $datac[10];
    $databmi[10];
    $dayi[10];
    $Username = $_SESSION['Username'];
    $sql = "SELECT w,h,bmi,waistline,days FROM polite_prescription_new WHERE Week=:week AND Username=:Username";
    $query = $dbh->prepare($sql);
    $query->bindParam(':Username', $Username, PDO::PARAM_STR);
    $query->bindParam(':week', $week, PDO::PARAM_STR);
    //  $query->bindParam(':Week',$Username,PDO::PARAM_STR);
    $query->execute();
    $results = $query->fetchAll(PDO::FETCH_OBJ);

    if ($query->rowCount() > 0) {
      $i = 0;
      foreach ($results as $result) {

        $dataw[$i] = $result->w;
        $datah[$i] = $result->h;
        $databmi[$i] = $result->bmi;
        $datac[$i] = $result->waistline;
        $dayi[$i] = $result->days;
        $i++;
      }
    }



    ?>
    <!DOCTYPE HTML>
    <html>

    <head>
      <script>
        window.onload = function() {

          var chart = new CanvasJS.Chart("chartContainer", {
            animationEnabled: true,
            title: {
              text: "น้ำหนัก ส่วนสูง รอบเอวและ BMI"
            },
            axisY: {
              title: "กิโลกรัม หรือ เซ็นติเมตร",
              titleFontColor: "#4F81BC",
              lineColor: "#4F81BC",
              labelFontColor: "#4F81BC",
              tickColor: "#4F81BC"
            },
            toolTip: {
              shared: true
            },
            legend: {
              cursor: "pointer",
              itemclick: toggleDataSeries
            },
            data: [{
                type: "column",
                name: "น้ำหนัก",
                legendText: "น้ำหนัก",
                showInLegend: true,
                dataPoints: [{
                    label: <?php echo $dayi[0] ?>,
                    y: <?php echo $dataw[0] ?>
                  },
                  {
                    label: <?php echo $dayi[1] ?>,
                    y: <?php echo $dataw[1] ?>
                  },
                  {
                    label: <?php echo $dayi[2] ?>,
                    y: <?php echo $dataw[2] ?>
                  },
                  {
                    label: <?php echo $dayi[3] ?>,
                    y: <?php echo $dataw[3] ?>
                  },
                  {
                    label: <?php echo $dayi[4] ?>,
                    y: <?php echo $dataw[4] ?>
                  },
                  {
                    label: <?php echo $dayi[5] ?>,
                    y: <?php echo $dataw[5] ?>
                  },
                  {
                    label: <?php echo $dayi[6] ?>,
                    y: <?php echo $dataw[6] ?>
                  },
                ]
              },
              {
                type: "column",
                name: "ส่วนสูง",
                legendText: "ส่วนสูง",
                showInLegend: true,
                dataPoints: [{
                    label: <?php echo $dayi[0] ?>,
                    y: <?php echo $datah[0] ?>
                  },
                  {
                    label: <?php echo $dayi[1] ?>,
                    y: <?php echo $datah[1] ?>
                  },
                  {
                    label: <?php echo $dayi[2] ?>,
                    y: <?php echo $datah[2] ?>
                  },
                  {
                    label: <?php echo $dayi[3] ?>,
                    y: <?php echo $datah[3] ?>
                  },
                  {
                    label: <?php echo $dayi[4] ?>,
                    y: <?php echo $datah[4] ?>
                  },
                  {
                    label: <?php echo $dayi[5] ?>,
                    y: <?php echo $datah[5] ?>
                  },
                  {
                    label: <?php echo $dayi[6] ?>,
                    y: <?php echo $datah[6] ?>
                  },
                ]
              },
              {
                type: "column",
                name: "รอบเอว",
                legendText: "รอบเอว",
                showInLegend: true,
                dataPoints: [{
                    label: <?php echo $dayi[0] ?>,
                    y: <?php echo $datac[0] ?>
                  },
                  {
                    label: <?php echo $dayi[1] ?>,
                    y: <?php echo $datac[1] ?>
                  },
                  {
                    label: <?php echo $dayi[2] ?>,
                    y: <?php echo $datac[2] ?>
                  },
                  {
                    label: <?php echo $dayi[3] ?>,
                    y: <?php echo $datac[3] ?>
                  },
                  {
                    label: <?php echo $dayi[4] ?>,
                    y: <?php echo $datac[4] ?>
                  },
                  {
                    label: <?php echo $dayi[5] ?>,
                    y: <?php echo $datac[5] ?>
                  },
                  {
                    label: <?php echo $dayi[6] ?>,
                    y: <?php echo $datac[6] ?>
                  },
                ]
              },
              {
                type: "column",
                name: "BMI",
                legendText: "BMI",
                showInLegend: true,
                dataPoints: [{
                    label: <?php echo $dayi[0] ?>,
                    y: <?php echo $databmi[0] ?>
                  },
                  {
                    label: <?php echo $dayi[1] ?>,
                    y: <?php echo $databmi[1] ?>
                  },
                  {
                    label: <?php echo $dayi[2] ?>,
                    y: <?php echo $databmi[2] ?>
                  },
                  {
                    label: <?php echo $dayi[3] ?>,
                    y: <?php echo $databmi[3] ?>
                  },
                  {
                    label: <?php echo $dayi[4] ?>,
                    y: <?php echo $databmi[4] ?>
                  },
                  {
                    label: <?php echo $dayi[5] ?>,
                    y: <?php echo $databmi[5] ?>
                  },
                  {
                    label: <?php echo $dayi[6] ?>,
                    y: <?php echo $databmi[6] ?>
                  },
                ]
              },
            ]
          });
          chart.render();

          function toggleDataSeries(e) {
            if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
              e.dataSeries.visible = false;
            } else {
              e.dataSeries.visible = true;
            }
            chart.render();
          }

        }
      </script>
    </head>
    <style>
      * {}
    </style>
    <?php include 'includes/headerwork.php'; ?>

  <body>
    <main>
      <div class="container">
        <br>
        <form method="post">
          <div class="row gx-5">
            <div class="col">
              <div class="p-3 "> <select class="form-select" aria-label="Default select example" name="daydd">

                  <?php
                  for ($x = $d1; $x <= $d2; $x++) {
                    if ($_POST['daydd'] == $x) {
                  ?>
                      <option value="<?php echo $x ?>" selected><?php echo $x ?></option>
                    <?php
                    } else {
                    ?>

                      <option value="<?php echo $x ?>"><?php echo $x ?></option>
                  <?php
                    }
                  }
                  ?>
                </select></div>
            </div>
            <div class="col">
              <div class="p-3 "><input type="submit" value="รายงาน" name="submitday" id="submitday" class="btn btn-warning"></div>
            </div>
          </div>
        </form>
        <div id="chartContainer" style="height: 500px; width: 100%; " class="mb-5"></div>
        <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>

        <?php
        if (isset($_POST['submitday'])) {
          $dates = $_POST['datepost'];
          $dgo = $_SESSION['Gdate'];
          function datediff($start, $end)
          {

            $datediff = strtotime(dateform($end)) - strtotime(dateform($start));
            return floor($datediff / (60 * 60 * 24));
          }

          function dateform($date)
          {

            $d = explode('-', $date);
            return $date;
          }

          $dategos = $_POST['daydd'];


          $sql = "SELECT * FROM polite_prescription_new WHERE days=:days AND Username=:Username";
          $query = $dbh->prepare($sql);
          $query->bindParam(':days', $dategos, PDO::PARAM_STR);
          $query->bindParam(':Username', $Username, PDO::PARAM_STR);
          $query->execute();
          $results = $query->fetchAll(PDO::FETCH_OBJ);
          if ($query->rowCount() > 0) {

            foreach ($results as $result) {

        ?>
              <div class="d-flex justify-content-center">

                <div class="d-flex flex-column" style="font-size: 18px;">
                  <div class="mt-2">

                    <h5>รายงานประจำวันที่วันที่ <?php echo htmlentities($result->days) ?> สัปดาห์ที่ <?php echo htmlentities($result->Week) ?></h5>

                    <div class="d-flex " style="background-color:rgba(196, 196, 196, 1);">
                      <h6 class="mx-2 my-2">
                        <?php echo date('d-m-Y', strtotime($dates)) ?>
                      </h6>
                    </div>
                  </div>

                  <div class="mt-2">

                    1. ลดอาหารกลุ่มคาร์โบไฮเดรต เช่น ข้าว ขนมปัง ขนมหวาน ( 3 มื้อ )

                    <div class=" d-flex justify-content-around " style="background-color:rgba(196, 196, 196, 1);">
                      <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="1" id="1" value="y" <?php if ($result->t1 == "y") {
                                                                                                  echo "checked";
                                                                                                } ?> required>
                        <label class="form-check-label" for="1">ทำ</label>
                      </div>
                      <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="1" id="inlineRadio2" value="n" <?php if ($result->t1 == "n") {
                                                                                                            echo "checked";
                                                                                                          } ?> required>
                        <label class="form-check-label" for="1">ไม่ทำ</label>
                      </div>

                    </div>
                  </div>
                  <div class="mt-2">
                    2. เพิ่มอาหารโปรตีนคุณภาพดี เช่น นม ไข่ต้ม ปลา ( 3มื้อ )
                    <div class=" d-flex justify-content-around" style="background-color:rgba(196, 196, 196, 1);">
                      <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="2" id="2" value="y" <?php if ($result->t2 == "y") {
                                                                                                  echo "checked";
                                                                                                } ?> required>
                        <label class="form-check-label" for="2">ทำ</label>
                      </div>
                      <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="2" id="inlineRadio2" value="n" <?php if ($result->t2 == "n") {
                                                                                                            echo "checked";
                                                                                                          } ?> required>
                        <label class="form-check-label" for="2">ไม่ทำ</label>
                      </div>

                    </div>
                  </div>
                  <div class="mt-2">
                    3. ไม่รับประทานอาหารเย็นหลัง 20.00 น.
                    <div class=" d-flex justify-content-around" style="background-color:rgba(196, 196, 196, 1);">
                      <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="3" id="3" value="y" <?php if ($result->t3 == "y") {
                                                                                                  echo "checked";
                                                                                                } ?> required>
                        <label class="form-check-label" for="3">ทำ</label>
                      </div>
                      <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="3" id="inlineRadio2" value="n" <?php if ($result->t3 == "n") {
                                                                                                            echo "checked";
                                                                                                          } ?> required>
                        <label class="form-check-label" for="3">ไม่ทำ</label>
                      </div>

                    </div>
                  </div>

                  <div class="mt-2">
                    4. กินข้าวเสร็จแล้วไม่กินขนมหวานต่อ หรือของกินเล่น ( 3มื้อ )
                    <div class="d-flex justify-content-around" style="background-color:rgba(196, 196, 196, 1);">
                      <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="4" id="4" value="y" <?php if ($result->t4 == "y") {
                                                                                                  echo "checked";
                                                                                                } ?> required>
                        <label class="form-check-label" for="4">ทำ</label>
                      </div>
                      <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="4" id="inlineRadio2" value="n" <?php if ($result->t4 == "n") {
                                                                                                            echo "checked";
                                                                                                          } ?> required>
                        <label class="form-check-label" for="4">ไม่ทำ</label>
                      </div>
                    </div>
                  </div>

                  <div class="mt-2">
                    5. ดื่มน้ำเปล่าวันละ 2 ลิตร หรือ 8 - 10 แก้ว
                    <div class="d-flex justify-content-around" style="background-color:rgba(196, 196, 196, 1);">
                      <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="5" id="5" value="y" <?php if ($result->t5 == "y") {
                                                                                                  echo "checked";
                                                                                                } ?> required>
                        <label class="form-check-label" for="5">ทำ</label>
                      </div>
                      <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="5" id="inlineRadio2" value="n" <?php if ($result->t5 == "n") {
                                                                                                            echo "checked";
                                                                                                          } ?> required>
                        <label class="form-check-label" for="5">ไม่ทำ</label>
                      </div>

                    </div>
                  </div>

                  <div class="mt-2">
                    6. เน้นอาหารต้ม นึ่ง ลดอาหาร ทอด หวาน มัน เค็ม ( 3 มื้อ )
                    <div class="d-flex justify-content-around" style="background-color:rgba(196, 196, 196, 1);">
                      <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="6" id="6" value="y" <?php if ($result->t6 == "y") {
                                                                                                  echo "checked";
                                                                                                } ?> required>
                        <label class="form-check-label" for="6">ทำ</label>
                      </div>
                      <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="6" id="inlineRadio2" value="n" <?php if ($result->t6 == "n") {
                                                                                                            echo "checked";
                                                                                                          } ?> required>
                        <label class="form-check-label" for="6">ไม่ทำ</label>
                      </div>

                    </div>
                  </div>

                  <div class="mt-2">
                    7. ออกกำลังกายได้ 20 - 30 นาที ต่อครั้ง
                    <div class="d-flex justify-content-around" style="background-color:rgba(196, 196, 196, 1);">
                      <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="7" id="7" value="y" <?php if ($result->t7 == "y") {
                                                                                                  echo "checked";
                                                                                                } ?> required>
                        <label class="form-check-label" for="7">ทำ</label>
                      </div>
                      <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="7" id="inlineRadio2" value="n" <?php if ($result->t7 == "n") {
                                                                                                            echo "checked";
                                                                                                          } ?> required>
                        <label class="form-check-label" for="7">ไม่ทำ</label>
                      </div>

                    </div>
                  </div>

                  <div class="mt-2">
                    8. ลักษณะออดกำลังกายมีเหงื่อซึม และหายใจเร็วกว่าปกติ
                    <div class="d-flex justify-content-around" style="background-color:rgba(196, 196, 196, 1);">
                      <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="8" id="8" value="y" <?php if ($result->t8 == "y") {
                                                                                                  echo "checked";
                                                                                                } ?> required>
                        <label class="form-check-label" for="8">ทำ</label>
                      </div>
                      <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="8" id="inlineRadio2" value="n" <?php if ($result->t8 == "n") {
                                                                                                            echo "checked";
                                                                                                          } ?> required>
                        <label class="form-check-label" for="8">ไม่ทำ</label>
                      </div>

                    </div>
                  </div>

                  <div class="mt-2">
                    9. ออกกำลังกายท่าบริหารกระชับเอว พุง
                    <div class="d-flex justify-content-around" style="background-color:rgba(196, 196, 196, 1);">
                      <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="9" id="9" value="y" <?php if ($result->t9 == "y") {
                                                                                                  echo "checked";
                                                                                                } ?> required>
                        <label class="form-check-label" for="9">ทำ</label>
                      </div>
                      <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="9" id="inlineRadio2" value="n" <?php if ($result->t9 == "n") {
                                                                                                            echo "checked";
                                                                                                          } ?> required>
                        <label class="form-check-label" for="9">ไม่ทำ</label>
                      </div>

                    </div>
                  </div>
                </div>



              </div>
              <div class="my-5">
                <div style="background-color:#CC9999"><label for="Textarea">
                    <p style="margin: 10px 10px; font-size:16px">ปัญหาอุปสรรค ในการเปลี่ยนแปลงของคุณ</p>
                  </label>
                  <?php


                  $sql = "SELECT * FROM polite_obstacle WHERE Username=:Username AND days=:days";
                  $query = $dbh->prepare($sql);
                  $query->bindParam(':Username', $Username, PDO::PARAM_STR);
                  $query->bindParam(':days', $Gday, PDO::PARAM_INT);
                  $query->execute();
                  $resultss = $query->fetchAll(PDO::FETCH_OBJ);

                  if ($query->rowCount() > 0) {

                    foreach ($resultss as $results) {
                  ?>
                      <textarea class="form-control" id="Textarea" name="Textarea" rows="3"> <?php echo htmlentities($results->details) ?> </textarea>


                  <?php
                    }
                  }

                  ?>

                </div>
                <br>
                <div class="form-row">
                  <div class="col-md-4">
                    <label for="weight">น้ำหนัก </label>
                    <input type="number" class="form-control" id="weight" name="weight" placeholder="weight" value="<?php echo (int)$result->w; ?>" oninput="calculate()" required>
                  </div>
                  <div class="col-md-4 offset-md-4">
                    <label for="height">ส่วนสูง</label>
                    <input type="number" class="form-control" id="height" name="height" placeholder="height" value="<?php echo (int)$result->h; ?>" oninput="calculate()" required>
                  </div>
                </div>
                <br>
                <div class="form-row">
                  <div class="col-md-4">
                    <label for="bmi">BMI</label>
                    <input type="text" class="form-control" id="bmi" name="bmi" placeholder="bmi" value="<?php echo (int)$result->bmi; ?>" disabled>

                  </div>
                  <div class="col-md-4 offset-md-4">
                    <label for="waistline">รอบเอว</label>
                    <input type="number" class="form-control" name="waistline" id="waistline" value="<?php echo (int)$result->waistline; ?>" placeholder="waistline" required>
                  </div>
                </div>
                <br>




              </div>
        <?php
            }
          }
        }
        ?>
      </div>



      </div>
    </main>

  </body>
  <?php include 'includes/footer.php' ?>

  </html>

<?php
}
?>