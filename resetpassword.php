
<?php
session_start();
error_reporting(0);
include 'includes/config.php';
if(isset($_POST['reset']))
{
$Username=$_POST['Username'];
$Password=md5($_POST['Password']);
$confirm_password=md5($_POST['confirm_password']);
if($Password!=$confirm_password){
    echo "<script>alert('รหัสผ่านผิดพลาดไม่ตรงกัน');</script>";
}else{


$sql ="UPDATE polite_user SET Password=:Password WHERE Username=:Username";
$query= $dbh -> prepare($sql);
$query-> bindParam(':Username', $Username, PDO::PARAM_STR);
$query-> bindParam(':Password', $Password, PDO::PARAM_STR);
$query-> execute();
header('location:login.php');
}
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Kanit:wght@100;200;300;400;500&family=Roboto:ital,wght@0,100;0,400;0,500;0,700;0,900;1,500;1,700;1,900&display=swap" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>เข้าสู่ระบบ</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <script src="includes/js/bootstrap.js"></script>  
</head>
<style>
  *{

  }

</style>
<?php include 'includes/header.php'; ?>

<body>
  <main>
<div class="p-3 mb-2 bg-secondary text-white"><div class="d-flex justify-content-between">
<div class="item" style="margin-right:  50px; font-size: 20px;">
                    <a href="#" class="nav-link  text-white">]ลืมรหัสผ่าน</a>
                </div>
<div class="item" style="margin-right:  50px; font-size: 20px;">
                    <a href="index.php" class="nav-link  text-white">หน้าหลัก</a>
                </div>
 </div> 
</div>
<div class="container">
  <br>
  <br>
  <br>
  <br>
  <form  method="post">
  <div class="row mb-3">
    <label for="Username" class="col-sm-2 col-form-label-lg ">Username</label>
    <div class="col-sm-10">
      <input type="number" class="form-control" id="Username" name="Username"  onKeyPress="if(this.value.length==5) return false;"   placeholder="กรอก Username " require>
    </div>
  </div>
  <div class="row mb-3">
    <label for="Password" class="col-sm-2 col-form-label-lg">Password</label>
    <div class="col-sm-10">
      <input type="password" class="form-control" id="Password"  placeholder="กรอก Password " name="Password" require>
    </div>
  </div>
  <div class="row mb-3">
    <label for="Password" class="col-sm-2 col-form-label-lg">confirm password</label>
    <div class="col-sm-10">
      <input type="password" class="form-control" id="Password"  placeholder="กรอก confirm password " name="confirm_password" require>
    </div>
  </div>

 
  <div class="text-center">
  <button type="submit" name="reset" class="btn-lg btn-success me-2">เปลี่ยนรหัสผ่าน</button>
   
    </div>
</form>

<div>
  <br>
  </main>
</body>
<footer>
<?php include 'includes/footer.php'?>
</footer>

</html>