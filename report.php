<?php
session_start();
error_reporting(0);
include 'includes/config.php';
if (strlen($_SESSION['Username']) == 0) {
    header('location:index.php');
} else {
    $Gday = $_SESSION['Gday'];
    if(isset($_POST['sub23'])) {
        $week32= $_POST['weekby'];
        header("location:dashboardweek.php?week=$week32");
        
    }
?>


    <head>
        <meta charset="UTF-8">
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Kanit:wght@100;200;300;400;500&family=Roboto:ital,wght@0,100;0,400;0,500;0,700;0,900;1,500;1,700;1,900&display=swap" rel="stylesheet">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>ABC Health Fitness</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <script src="includes/js/bootstrap.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.bundle.min.js"></script>
        <?php
        $Username = $_SESSION['Username'];
        ?>
        <!DOCTYPE HTML>
        <html>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.bundle.min.js"></script>
        <?php
        $dataq[10];
        $datay[10];
        $datan[10];

        $sql = "SELECT SUM(t1='q') as t1q, SUM(t1='y') as t1y,SUM(t1='n') as t1n,SUM(t2='q') as t2q, SUM(t2='y') as t2y,SUM(t2='n') as t2n,
 SUM(t3='q') as t3q, SUM(t3='y') as t3y,SUM(t3='n') as t3n,
 SUM(t4='q') as t4q, SUM(t1='y') as t4y,SUM(t4='n') as t4n,
 SUM(t5='q') as t5q, SUM(t5='y') as t5y,SUM(t5='n') as t5n,
 SUM(t6='q') as t6q, SUM(t6='y') as t6y,SUM(t6='n') as t6n,
 SUM(t7='q') as t7q, SUM(t7='y') as t7y,SUM(t1='n') as t7n,
 SUM(t8='q') as t8q, SUM(t8='y') as t8y,SUM(t8='n') as t8n,
 SUM(t9='q') as t9q, SUM(t9='y') as t9y,SUM(t9='n') as t9n
 FROM `polite_prescription_new` WHERE Username=:Username";
        $query = $dbh->prepare($sql);
        $query->bindParam(':Username', $Username, PDO::PARAM_STR);
        $query->execute();
        $results = $query->fetchAll(PDO::FETCH_OBJ);

        if ($query->rowCount() > 0) {
            foreach ($results as $result) {

                $dataq[1] = $result->t1q;
                $dataq[2] = $result->t2q;
                $dataq[3] = $result->t3q;
                $dataq[4] = $result->t4q;
                $dataq[5] = $result->t5q;
                $dataq[6] = $result->t6q;
                $dataq[7] = $result->t7q;
                $dataq[8] = $result->t8q;
                $dataq[9] = $result->t9q;
                $datay[1] = $result->t1y;
                $datay[2] = $result->t2y;
                $datay[3] = $result->t3y;
                $datay[4] = $result->t4y;
                $datay[5] = $result->t5y;
                $datay[6] = $result->t6y;
                $datay[7] = $result->t7y;
                $datay[8] = $result->t8y;
                $datay[9] = $result->t9y;
                $datan[1] = $result->t1n;
                $datan[2] = $result->t2n;
                $datan[3] = $result->t3n;
                $datan[4] = $result->t4n;
                $datan[5] = $result->t5n;
                $datan[6] = $result->t6n;
                $datan[7] = $result->t7n;
                $datan[8] = $result->t8n;
                $datan[9] = $result->t9n;
            }
        }
        $dataPoints1 = array(
            array("label" => "1", "y" => $dataq[1]),
            array("label" => "2", "y" => $dataq[2]),
            array("label" => "3", "y" => $dataq[3]),
            array("label" => "4", "y" => $dataq[4]),
            array("label" => "5", "y" => $dataq[5]),
            array("label" => "6", "y" => $dataq[6]),
            array("label" => "7", "y" => $dataq[7]),
            array("label" => "8", "y" => $dataq[8]),
            array("label" => "9", "y" => $dataq[9]),
        );

        $dataPoints2 = array(
            array("label" => "1", "y" => $datay[1]),
            array("label" => "2", "y" => $datay[2]),
            array("label" => "3", "y" => $datay[3]),
            array("label" => "4", "y" => $datay[4]),
            array("label" => "5", "y" => $datay[5]),
            array("label" => "6", "y" => $datay[6]),
            array("label" => "7", "y" => $datay[7]),
            array("label" => "8", "y" => $datay[8]),
            array("label" => "9", "y" => $datay[9]),
        );

        $dataPoints3 = array(
            array("label" => "1", "y" => $datan[1]),
            array("label" => "2", "y" => $datan[2]),
            array("label" => "3", "y" => $datan[3]),
            array("label" => "4", "y" => $datan[4]),
            array("label" => "5", "y" => $datan[5]),
            array("label" => "6", "y" => $datan[6]),
            array("label" => "7", "y" => $datan[7]),
            array("label" => "8", "y" => $datan[8]),
            array("label" => "9", "y" => $datan[9]),
        );


        ?>
    </head>
    <style>
        * {}
    </style>

    <!DOCTYPE html>
    <html lang="en">
    <?php include 'includes/headerwork.php'; ?>

    <body>
        <div class="p-3 mb-2 bg-secondary text-white">
            <div class="d-flex justify-content-between">
                <div class="item" style="margin-right:  50px; ">
                    <a href="#" class="nav-link  text-white">รายงานผลการปฏิบัติ</a>
                </div>
                <div class="item" style="margin-right:  50px; ">
                    <a href="index.php" class="nav-link  text-white">หน้าหลัก</a>
                </div>
            </div>
        </div>


        <div class="container">

            <br>
            <?php

            if (isset($_POST['dayday'])) {
                $dates = $_POST['datepost'];
                $dgo = $_SESSION['Gdate'];
                function datediff($start, $end)
                {

                    $datediff = strtotime(dateform($end)) - strtotime(dateform($start));
                    return floor($datediff / (60 * 60 * 24));
                }

                function dateform($date)
                {

                    $d = explode('-', $date);
                    return $date;
                }

                $dategos = datediff($dgo, $dates) + 1;


                $sql = "SELECT * FROM polite_prescription_new WHERE days=:days AND Username=:Username";
                $query = $dbh->prepare($sql);
                $query->bindParam(':days', $dategos, PDO::PARAM_STR);
                $query->bindParam(':Username', $Username, PDO::PARAM_STR);
                $query->execute();
                $results = $query->fetchAll(PDO::FETCH_OBJ);
                if ($query->rowCount() > 0) {

                    foreach ($results as $result) {

            ?>
                        <div class="d-flex justify-content-center">

                            <div class="d-flex flex-column" style="font-size: 18px;">
                                <div class="mt-2">

                                    <h5>รายงานประจำวันที่วันที่ <?php echo htmlentities($result->days) ?> สัปดาห์ที่ <?php echo htmlentities($result->Week) ?></h5>

                                    <div class="d-flex " style="background-color:rgba(196, 196, 196, 1);">
                                        <h6 class="mx-2 my-2">
                                            <?php echo date('d-m-Y', strtotime($dates)) ?>
                                        </h6>
                                    </div>
                                </div>

                                <div class="mt-2">

                                    1. ลดอาหารกลุ่มคาร์โบไฮเดรต เช่น ข้าว ขนมปัง ขนมหวาน ( 3 มื้อ )

                                    <div class=" d-flex justify-content-around " style="background-color:rgba(196, 196, 196, 1);">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="1" id="1" value="y" <?php if ($result->t1 == "y") {
                                                                                                                        echo "checked";
                                                                                                                    } ?> required>
                                            <label class="form-check-label" for="1">ทำ</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="1" id="inlineRadio2" value="n" <?php if ($result->t1 == "n") {
                                                                                                                                    echo "checked";
                                                                                                                                } ?> required>
                                            <label class="form-check-label" for="1">ไม่ทำ</label>
                                        </div>

                                    </div>
                                </div>
                                <div class="mt-2">
                                    2. เพิ่มอาหารโปรตีนคุณภาพดี เช่น นม ไข่ต้ม ปลา ( 3มื้อ )
                                    <div class=" d-flex justify-content-around" style="background-color:rgba(196, 196, 196, 1);">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="2" id="2" value="y" <?php if ($result->t2 == "y") {
                                                                                                                        echo "checked";
                                                                                                                    } ?> required>
                                            <label class="form-check-label" for="2">ทำ</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="2" id="inlineRadio2" value="n" <?php if ($result->t2 == "n") {
                                                                                                                                    echo "checked";
                                                                                                                                } ?> required>
                                            <label class="form-check-label" for="2">ไม่ทำ</label>
                                        </div>

                                    </div>
                                </div>
                                <div class="mt-2">
                                    3. ไม่รับประทานอาหารเย็นหลัง 20.00 น.
                                    <div class=" d-flex justify-content-around" style="background-color:rgba(196, 196, 196, 1);">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="3" id="3" value="y" <?php if ($result->t3 == "y") {
                                                                                                                        echo "checked";
                                                                                                                    } ?> required>
                                            <label class="form-check-label" for="3">ทำ</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="3" id="inlineRadio2" value="n" <?php if ($result->t3 == "n") {
                                                                                                                                    echo "checked";
                                                                                                                                } ?> required>
                                            <label class="form-check-label" for="3">ไม่ทำ</label>
                                        </div>

                                    </div>
                                </div>

                                <div class="mt-2">
                                    4. กินข้าวเสร็จแล้วไม่กินขนมหวานต่อ หรือของกินเล่น ( 3มื้อ )
                                    <div class="d-flex justify-content-around" style="background-color:rgba(196, 196, 196, 1);">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="4" id="4" value="y" <?php if ($result->t4 == "y") {
                                                                                                                        echo "checked";
                                                                                                                    } ?> required>
                                            <label class="form-check-label" for="4">ทำ</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="4" id="inlineRadio2" value="n" <?php if ($result->t4 == "n") {
                                                                                                                                    echo "checked";
                                                                                                                                } ?> required>
                                            <label class="form-check-label" for="4">ไม่ทำ</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="mt-2">
                                    5. ดื่มน้ำเปล่าวันละ 2 ลิตร หรือ 8 - 10 แก้ว
                                    <div class="d-flex justify-content-around" style="background-color:rgba(196, 196, 196, 1);">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="5" id="5" value="y" <?php if ($result->t5 == "y") {
                                                                                                                        echo "checked";
                                                                                                                    } ?> required>
                                            <label class="form-check-label" for="5">ทำ</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="5" id="inlineRadio2" value="n" <?php if ($result->t5 == "n") {
                                                                                                                                    echo "checked";
                                                                                                                                } ?> required>
                                            <label class="form-check-label" for="5">ไม่ทำ</label>
                                        </div>

                                    </div>
                                </div>

                                <div class="mt-2">
                                    6. เน้นอาหารต้ม นึ่ง ลดอาหาร ทอด หวาน มัน เค็ม ( 3 มื้อ )
                                    <div class="d-flex justify-content-around" style="background-color:rgba(196, 196, 196, 1);">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="6" id="6" value="y" <?php if ($result->t6 == "y") {
                                                                                                                        echo "checked";
                                                                                                                    } ?> required>
                                            <label class="form-check-label" for="6">ทำ</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="6" id="inlineRadio2" value="n" <?php if ($result->t6 == "n") {
                                                                                                                                    echo "checked";
                                                                                                                                } ?> required>
                                            <label class="form-check-label" for="6">ไม่ทำ</label>
                                        </div>

                                    </div>
                                </div>

                                <div class="mt-2">
                                    7. ออกกำลังกายได้ 20 - 30 นาที ต่อครั้ง
                                    <div class="d-flex justify-content-around" style="background-color:rgba(196, 196, 196, 1);">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="7" id="7" value="y" <?php if ($result->t7 == "y") {
                                                                                                                        echo "checked";
                                                                                                                    } ?> required>
                                            <label class="form-check-label" for="7">ทำ</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="7" id="inlineRadio2" value="n" <?php if ($result->t7 == "n") {
                                                                                                                                    echo "checked";
                                                                                                                                } ?> required>
                                            <label class="form-check-label" for="7">ไม่ทำ</label>
                                        </div>

                                    </div>
                                </div>

                                <div class="mt-2">
                                    8. ลักษณะออดกำลังกายมีเหงื่อซึม และหายใจเร็วกว่าปกติ
                                    <div class="d-flex justify-content-around" style="background-color:rgba(196, 196, 196, 1);">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="8" id="8" value="y" <?php if ($result->t8 == "y") {
                                                                                                                        echo "checked";
                                                                                                                    } ?> required>
                                            <label class="form-check-label" for="8">ทำ</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="8" id="inlineRadio2" value="n" <?php if ($result->t8 == "n") {
                                                                                                                                    echo "checked";
                                                                                                                                } ?> required>
                                            <label class="form-check-label" for="8">ไม่ทำ</label>
                                        </div>

                                    </div>
                                </div>

                                <div class="mt-2">
                                    9. ออกกำลังกายท่าบริหารกระชับเอว พุง
                                    <div class="d-flex justify-content-around" style="background-color:rgba(196, 196, 196, 1);">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="9" id="9" value="y" <?php if ($result->t9 == "y") {
                                                                                                                        echo "checked";
                                                                                                                    } ?> required>
                                            <label class="form-check-label" for="9">ทำ</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="9" id="inlineRadio2" value="n" <?php if ($result->t9 == "n") {
                                                                                                                                    echo "checked";
                                                                                                                                } ?> required>
                                            <label class="form-check-label" for="9">ไม่ทำ</label>
                                        </div>

                                    </div>
                                </div>
                            </div>



                        </div>
                        <div class="my-5">
                            <div style="background-color:#CC9999"><label for="Textarea">
                                    <p style="margin: 10px 10px; font-size:16px">ปัญหาอุปสรรค ในการเปลี่ยนแปลงของคุณ</p>
                                </label>
                                <?php


                                $sql = "SELECT * FROM polite_obstacle WHERE Username=:Username AND days=:days";
                                $query = $dbh->prepare($sql);
                                $query->bindParam(':Username', $Username, PDO::PARAM_STR);
                                $query->bindParam(':days', $Gday, PDO::PARAM_INT);
                                $query->execute();
                                $resultss = $query->fetchAll(PDO::FETCH_OBJ);

                                if ($query->rowCount() > 0) {

                                    foreach ($resultss as $results) {
                                ?>
                                        <textarea class="form-control" id="Textarea" name="Textarea" rows="3"> <?php echo htmlentities($results->details) ?> </textarea>


                                <?php
                                    }
                                }

                                ?>

                            </div>
                            <br>
                            <div class="form-row">
                                <div class="col-md-4">
                                    <label for="weight">น้ำหนัก </label>
                                    <input type="number" class="form-control" id="weight" name="weight" placeholder="weight" value="<?php echo (int)$result->w; ?>" oninput="calculate()" required>
                                </div>
                                <div class="col-md-4 offset-md-4">
                                    <label for="height">ส่วนสูง</label>
                                    <input type="number" class="form-control" id="height" name="height" placeholder="height" value="<?php echo (int)$result->h; ?>" oninput="calculate()" required>
                                </div>
                            </div>
                            <br>
                            <div class="form-row">
                                <div class="col-md-4">
                                    <label for="bmi">BMI</label>
                                    <input type="text" class="form-control" id="bmi" name="bmi" placeholder="bmi" value="<?php echo (int)$result->bmi; ?>" disabled>

                                </div>
                                <div class="col-md-4 offset-md-4">
                                    <label for="waistline">รอบเอว</label>
                                    <input type="number" class="form-control" name="waistline" id="waistline" value="<?php echo (int)$result->waistline; ?>" placeholder="waistline" required>
                                </div>
                            </div>
                            <br>




                        </div>
            <?php
                    }
                }
            }
            ?>

            <div class="d-flex flex-column justify-content-center">
                <div class="d-flex justify-content-center">

                    <form method="POST">
                        <div class="input-group my-3">
                            <span class="input-group-text" id="inputGroup-sizing-default">รายงานประจำวันนี้</span>
                            <?php

                            $date_plus = date('Y-m-d', strtotime($_SESSION['Gdate'] . '+84 days'));
                            ?>
                            <input type="date" class="form-control" name="datepost" aria-label="Sizing example input" max="<?php echo $date_plus ?>" min="<?php echo $_SESSION['Gdate'] ?>" aria-describedby="inputGroup-sizing-default">
                        </div>

                        <div class="col text-center">
                            <button type="submit" class="btn btn-warning btn-l" name="dayday" id="dayday">รายงาน</button>
                        </div>
                        
                        <div class="input-group my-3">
                            <?php $dayst = ceil($Gday / 7);
                            $d1 = ($dayst - 1) * 7;
                            $d2 = ($dayst * 7) - 1;

                            ?>
                           
                                <span class="input-group-text" id="">รายงานสัปดาห์ที่</span>

                                <!-- <input type="text" class="form-control" value="<?php echo ceil($Gday / 7) ?> " name="dated2" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" disabled> -->
                                <select class="form-select" aria-label="Default select example" name="weekby" onchange="weekbyFunction()" id="weekby">

                                    <?php
                                    for ($x = 1; $x <= $dayst; $x++) {
                                        if ($x == $dayst) {
                                    ?>
                                            <option value="<?php echo $dayst ?>" selected><?php echo $dayst ?></option>
                                        <?php
                                        } else {
                                        ?>
                                            <option value="<?php echo $x ?>"><?php echo $x ?></option>
                                    <?php
                                        }
                                    }
                                    ?>

                                </select>
                                <input type="text" class="form-control" value=" <?php echo date('d-m-Y', strtotime("+ {$d1} day", strtotime($_SESSION['Gdate']))) ?>" id="daybyjs1" name="dated2" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" disabled>
                                <span class="input-group-text" id="">ถึง</span>
                                <input type="text" class="form-control" value=" <?php echo date('d-m-Y', strtotime("+ {$d2} day", strtotime($_SESSION['Gdate']))) ?> " id="daybyjs2" name="dated2" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" disabled>
                        </div>
                        <div class="col text-center">
                            <input type="submit" value="รายงาน" name="sub23"  name="sub23" id="sub23" class="btn btn-warning">

                        </div>

                        <div class="input-group my-3">

                            <span class="input-group-text" id="">รายงานครบ12สัปดาห์</span>
                            <input type="text" class="form-control" value="<?php echo date('d-m-Y', strtotime($_SESSION['Gdate'])) ?>" name="dated2" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" disabled>
                            <span class="input-group-text" id="">ถึง</span>
                            <input type="text" class="form-control" value=" <?php echo date('d-m-Y', strtotime('+83 days')) ?> " name="dated2" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default" disabled>
                        </div>
                </div>
                <div class="col text-center">

                    <a href="dashboardw.php"><button type="button" class="btn btn-warning">รายงาน</button></a>
                </div>
                <br>
                <br>
                </form>
            </div>
        </div>

        </div>

        <script>
            function weekbyFunction() {
                var x = document.getElementById("weekby").value;
                //datejs.setDate(date.getDate() + 1);
                <?php
                $orderdate = explode('-', $_SESSION['Gdate']);
                $year = $orderdate[0];
                $month = $orderdate[1];
                $day  = $orderdate[2];
                ?>
                var dd1 = (x - 1) * 7;
                var dd2 = (x * 7) - 1;
                const dss = new Date("<?php echo $year . "-" . $month . "-" . $day ?>");
                var dsss1 = new Date("<?php echo $year . "-" . $month . "-" . $day ?>");
                var dsss2 = new Date("<?php echo $year . "-" . $month . "-" . $day ?>");
                // dss.setDate(date.getDate() + dd1);
                // dsss2.setDate(date.getDate() + dd2);
                dsss1.setDate(dsss1.getDate() + dd1);
                dsss2.setDate(dsss2.getDate() + dd2);
                console.log(dsss1);
                console.log(dsss2);



                var dd = dsss1.getDate();

                var mm = dsss1.getMonth() + 1;
                var yyyy = dsss1.getFullYear();
                if (dd < 10) {
                    dd = '0' + dd;
                }

                if (mm < 10) {
                    mm = '0' + mm;
                }
                dsss1 = dd + '-' + mm + '-' + yyyy;
                document.getElementById("daybyjs1").value = dsss1;
                var dd = dsss2.getDate();

                var mm = dsss2.getMonth() + 1;
                var yyyy = dsss2.getFullYear();
                if (dd < 10) {
                    dd = '0' + dd;
                }

                if (mm < 10) {
                    mm = '0' + mm;
                }
                dsss2 = dd + '-' + mm + '-' + yyyy;
                document.getElementById("daybyjs2").value = dsss2;

            }
        </script>
    </body>
    <?php include 'includes/footer.php' ?>

    </html>

<?php
}
?>