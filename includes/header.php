<head>
    <meta charset="UTF-8">
    <link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Kanit:wght@100;200;300;400;500&family=Roboto:ital,wght@0,100;0,400;0,500;0,700;0,900;1,500;1,700;1,900&display=swap" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <script src="includes/js/bootstrap.js"></script>  
</head>
<style>
  *{
    font-family: 'Kanit', sans-serif;
  }
  

</style>



<header>
    <div style="margin:10px 30px;">
    <h1 style="font-size:20;margin-:30px;">ระบบการจัดการใบสั่งสุขภาพร่างกายของ ABC Health Fitness ผ่านเว็บแบบตอบสนอง</h1>
        </div>

    <!-- Navbar Color -->
    <nav class="navbar navbar-expand-sm navbar-dark bg-white">
        <!-- Content -->
        <div class="container-fluid">
            <!-- Brand -->
            <a href="#" class="navbar-brand text-danger" style="font-size: 30px; ">ABC Health Fitness </a>
            <!-- Menu -->
            <ul class="navbar-nav ">
                <li class="nav-item" style="margin-right:  50px; font-size: 20px;">
                    <a href="index.php" class="nav-link text-dark">หน้าหลัก</a>
                </li>
                <li class="nav-item" style="margin-right:  50px; font-size: 20px;">
                    <a href="whatisanorder.php" class="nav-link  text-dark">ใบสั่งสุขภาพคืออะไร</a>
                </li>
                <li class="nav-item" style="margin-right:  50px; font-size: 20px;">
                    <a href="knowledge.php" class="nav-link  text-dark">องค์ความรู้</a>
                </li>
                <li class="nav-item" style="margin-right:  50px; font-size: 20px;">
                    <a href="contact.php" class="nav-link text-dark">ติดต่อเรา</a>
                </li>
                <li class="nav-item" style="margin-right:  50px; font-size: 20px;">
                    <a href="login.php" class="nav-link text-dark">เข้าสู่ระบบ</a>
                </li>
                <li class="nav-item" style="margin-right:  50px;font-size: 20px;">
                    <a href="adminlogin.php" class="nav-link text-dark">สำหรับเจ้าหน้าที่</a>
                </li>
            </ul>
        </div>
    </nav>
    

</header>