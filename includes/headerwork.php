<?php
session_start();
error_reporting(0);
?>
<head>
    <meta charset="UTF-8">
    <link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Kanit:wght@100;200;300;400;500&family=Roboto:ital,wght@0,100;0,400;0,500;0,700;0,900;1,500;1,700;1,900&display=swap" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Carousel</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <script src="includes/js/bootstrap.js"></script>  
</head>
<style>
  *{
    font-family: 'Kanit', sans-serif;
  }

</style>



<header>
<div style="margin:10px 30px;">
    <h1 style="margin-:30px;">ระบบการจัดการใบสั่งสุขภาพร่างกายของ ABC Health Fitness ผ่านเว็บแบบตอบสนอง</h1>
        </div>

 <div style="margin:10px 30px;">
    <!-- Navbar Color -->
    <nav class="navbar navbar-expand-sm navbar-dark bg-white">
    
        <!-- Content -->
        <div class="container-fluid">
        
            <!-- Brand -->
            <a href="#" class="navbar-brand text-danger" style="font-size: 30px; ">ABC Health Fitness </a>
            <!-- Menu -->
            <ul class="navbar-nav ">
                
                <li class="nav-item" style="margin-right:  50px; font-size: 20px;">
                    <p  class="nav-link text-dark">Username : <?php echo $_SESSION['Username'] ?> </p>
                </li>
                <li class="nav-item" style="margin-right:  50px; font-size: 20px;">
                    <a href="performancereport.php" class="nav-link text-dark">หน้าหลัก</a>
                </li>
                <li class="nav-item" style="margin-right:  50px; font-size: 20px;">
                    <a href="report.php" class="nav-link text-dark">รายงานผลการปฏิบัติ</a>
            
                </li>
                <li class="nav-item" style="margin-right:  50px; font-size: 20px;">
                    <a href="slider/slider.php" class="nav-link text-dark">ข้อมูลแนะนำ</a>
                </li>
                <!-- <li class="nav-item" style="margin-right:  50px; font-size: 20px;">
                    <a href="dashboard.php" class="nav-link text-dark">dashboard</a>
                    
                </li> -->
                <li class="nav-item" style="margin-right:  50px; font-size: 20px;">
                    <a  href="changepassword.php" class="nav-link text-dark">เปลี่ยนรหัสผ่าน</a>
                </li>
                <li class="nav-item" style="margin-right:  50px;font-size: 20px;">
                    <a href="./logout.php" class="nav-link text-dark">ออกจากระบบ</a>
                </li>
            </ul>
        </div>
    </nav>
    

</header>