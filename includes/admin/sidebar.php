<head>
<meta charset="utf-8">
<link rel="stylesheet" href="polite\includes\css\styles.css">
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Kanit:wght@100;200;300;400;500&family=Roboto:ital,wght@0,100;0,400;0,500;0,700;0,900;1,500;1,700;1,900&display=swap" rel="stylesheet">
<meta name="viewport" content="width=device-width, initial-scale=1">

</head>
<style>
</style>
<nav>
<ul class="nav flex-column">
  <li class="nav-item">
    <a class="nav-link active text-white" aria-current="page" href="index.php"><h5>จัดการประชาสัมพันธ์</h5></a>
  </li>
  <li class="nav-item">
  <a class="nav-link active text-white" aria-current="page" href="knowledge.php"><h5>จัดการองค์ความรู้</h5></a>
  </li>
  <li class="nav-item">
  <a class="nav-link active text-white" aria-current="page" href="adduser.php"><h5>เพิ่มข้อมูลสมาชิก</h5></a>
  </li>
  <li class="nav-item">
  <a class="nav-link active text-white" aria-current="page" href="manageuser.php"><h5>จัดการข้อมูลสมาชิก</h5></a>
  </li>
  <li class="nav-item">
  <a class="nav-link active text-white" aria-current="page" href="recommend.php"><h5>ข้อมูลแนะนำ</h5></a>
  </li>
  <li class="nav-item">
  <a class="nav-link active text-white" aria-current="page" href="performancereport.php"><h5>รายงานผลการปฏิบัติ </h5></a>
  </li>
  <li class="nav-item">
  <a class="nav-link active text-white" aria-current="page" href="changepassword.php"><h5>เปลี่ยนรหัสผ่าน</h5></a>
  </li>
  <li class="nav-item">
  <a class="nav-link active text-danger" aria-current="page" href="logout.php"><h5>ออกจากระบบ</h5></a>
  </li>
</ul>
  </nav>